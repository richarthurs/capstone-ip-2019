`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Richard Arthurs
// 
// Create Date: 02/10/2019 04:11:13 PM
// Design Name: 
// Module Name: cam_fifo_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
/*
    - simple testbench to demonstrate the camera capture and reading from the FIFO
*/
//////////////////////////////////////////////////////////////////////////////////


module cam_fifo_tb();    
    localparam WIDTH = 8;   // image data width

    // Generated
    reg clk;
    reg pclk;
    reg[WIDTH-1:0] data_fake;
    reg href;
    reg vsync;
    reg trigger;
    
    // Outputs from camera, input to FIFO
    wire valid;
    wire[WIDTH-1:0] data_out; 
    wire pclk_out;
    
    // Outputs from capture
    wire cam_busy;
    
    // FIFO signals - inputs to fifo
    reg fifo_wrst_n;        
    reg fifo_rrst_n;
    reg fifo_rd;
    
    // Outputs from FIFO
    wire fifo_full;             // full flag
    wire[WIDTH-1:0] fifo_data;  // read data from FIFO
    wire fifo_empty;            // empty flag
        
    
    // Camera Interface Instantiation
    camera_if#(
        30,     // After this many pclks with href low, we will go idle again
        WIDTH   // data width 
    ) cam_inst(
        .clk(clk),
        .pclk(pclk),
        .data(data_fake),
        .href(href),
        .vsync(vsync),
        .valid(valid),
        .trigger(trigger),
        .data_out(data_out),
        .pclk_out(pclk_out),
        .busy_out(cam_busy)
    );
    
    
    // FIFO Instantiation
    buff#(
        WIDTH,  // data width
        2      // number of slots
     )buf_inst(
        .i_wclk(pclk_out),
        .i_wrst_n(fifo_wrst_n),
        .i_wr(valid),
        .i_wdata(data_out[WIDTH-1:0]),
        .i_rclk(clk),
        .i_rrst_n(fifo_rrst_n),
        .i_rd(fifo_rd),
        .o_wfull(fifo_full),
        .o_rdata(fifo_data[WIDTH-1:0]),
        .o_rempty(fifo_empty)
    );    
    
    
    
    // Generate the clocks
    // 50 MHz System Clock
    always begin
        clk <= 1'b1;
        #10;
        clk <= ~clk;
        #10;
    end
    
    // 32 MHz Pclk
    always begin
        pclk <= 1'b1;
        #15.625;
        pclk <= ~pclk;
        #15.625;
    end
    
    // Simulation Variables
    reg[3:0] img_count;
    reg[3:0] col_count;
    reg[3:0] row_count;
    reg[4:0] newline_href;
    reg[10:0] post_frame;
    
    // Main Testing Loop
    initial begin
        // Initialize
        vsync <= 1'b0;
        href <= 1'b0;
        data_fake <= 'b0;
        trigger <= 1'b0;
        fifo_wrst_n <= 1'b0;
        fifo_rrst_n <= 1'b0;
        fifo_rd <= 1'b0;
        
        // Slight delay
        repeat(3) @(posedge clk);
        
        // Enable the fifo
        fifo_wrst_n <= 1'b1;
        fifo_rrst_n <= 1'b1;
        
        // Loop through several image capture requests
        for(img_count = 0; img_count < 3; img_count = img_count + 1) begin
                    
            // Trigger
            trigger <= 1'b1;
            repeat(4) @(posedge pclk);
    
            // -----  Image Loop  -----
            
            // Vsync Pulse
            vsync <= 1'b0;
            repeat(3) @(posedge pclk);
            vsync <= 1'b1;
            
            // Pre-frame delay
            repeat(5) @(posedge pclk);
    
            // Send out a few lines
            for(row_count = 0; row_count < 3; row_count = row_count + 1) begin
                // Pixel Data - pump out a few pixels in the line
                for(col_count = 0; col_count < 5; col_count = col_count + 1) begin
                    href <= 1'b1;
                    data_fake <= data_fake + 1;
                    @(posedge pclk);
                end
            
                // Href low for a few clocks
                for(newline_href = 0; newline_href < 10; newline_href = newline_href + 1) begin
                    href <= 1'b0;
                    @(posedge pclk);
                end
            end
            
            // Post frame delay. The camera module parameter POST_FRAME_PCLK_THRESH sets the threshold for this. 50 for simulation, 500 IRL. 
            for(post_frame = 0; post_frame < 50; post_frame = post_frame + 1) begin
                @(posedge pclk);
            end
            
            // Deassert the trigger and also read from the FIFO
            repeat(4) @(posedge clk);
            trigger <= 1'b0;
            fifo_rd <= 1'b1;            // Assert read so data will come out of the fifo
            repeat(16) @(posedge clk);  // Pump out 16 bytes - should be exactly the frame written
            fifo_rd <= 1'b0;            // Done with reading (FIFO empty should go high for a while)
                   
        end // Image loop
        repeat(5) @(posedge clk);

    end // testing loop
endmodule


