// -----------------------------------------------------------------------------
// 'ctrl' Register Definitions
// Revision: 13
// -----------------------------------------------------------------------------
// Generated on 2019-02-13 at 19:12 (UTC) by airhdl version 2019.01.1
// -----------------------------------------------------------------------------
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
// POSSIBILITY OF SUCH DAMAGE.
// -----------------------------------------------------------------------------

#ifndef CTRL_H
#define CTRL_H

/* Revision number of the 'ctrl' register map */
#define CTRL_REVISION 13

/* Default base address of the 'ctrl' register map */
#define CTRL_DEFAULT_BASEADDR 0x00000000

/* Register 'control' */
#define CONTROL_OFFSET 0x00000000 /* address offset of the 'control' register */

/* Field  'control.cam_trigger' */
#define CONTROL_CAM_TRIGGER_BIT_OFFSET 0 /* bit offset of the 'cam_trigger' field */
#define CONTROL_CAM_TRIGGER_BIT_WIDTH 1 /* bit width of the 'cam_trigger' field */
#define CONTROL_CAM_TRIGGER_BIT_MASK 0x00000001 /* bit mask of the 'cam_trigger' field */
#define CONTROL_CAM_TRIGGER_RESET 0x0 /* reset value of the 'cam_trigger' field */

/* Field  'control.export_trigger' */
#define CONTROL_EXPORT_TRIGGER_BIT_OFFSET 1 /* bit offset of the 'export_trigger' field */
#define CONTROL_EXPORT_TRIGGER_BIT_WIDTH 1 /* bit width of the 'export_trigger' field */
#define CONTROL_EXPORT_TRIGGER_BIT_MASK 0x00000002 /* bit mask of the 'export_trigger' field */
#define CONTROL_EXPORT_TRIGGER_RESET 0x0 /* reset value of the 'export_trigger' field */

/* Register 'status' */
#define STATUS_OFFSET 0x00000004 /* address offset of the 'status' register */

/* Field  'status.cam_capturing' */
#define STATUS_CAM_CAPTURING_BIT_OFFSET 0 /* bit offset of the 'cam_capturing' field */
#define STATUS_CAM_CAPTURING_BIT_WIDTH 1 /* bit width of the 'cam_capturing' field */
#define STATUS_CAM_CAPTURING_BIT_MASK 0x00000001 /* bit mask of the 'cam_capturing' field */
#define STATUS_CAM_CAPTURING_RESET 0x0 /* reset value of the 'cam_capturing' field */

/* Field  'status.exporting' */
#define STATUS_EXPORTING_BIT_OFFSET 1 /* bit offset of the 'exporting' field */
#define STATUS_EXPORTING_BIT_WIDTH 1 /* bit width of the 'exporting' field */
#define STATUS_EXPORTING_BIT_MASK 0x00000002 /* bit mask of the 'exporting' field */
#define STATUS_EXPORTING_RESET 0x0 /* reset value of the 'exporting' field */

/* Field  'status.fifo_empty' */
#define STATUS_FIFO_EMPTY_BIT_OFFSET 2 /* bit offset of the 'fifo_empty' field */
#define STATUS_FIFO_EMPTY_BIT_WIDTH 1 /* bit width of the 'fifo_empty' field */
#define STATUS_FIFO_EMPTY_BIT_MASK 0x00000004 /* bit mask of the 'fifo_empty' field */
#define STATUS_FIFO_EMPTY_RESET 0x0 /* reset value of the 'fifo_empty' field */

/* Field  'status.fifo_full' */
#define STATUS_FIFO_FULL_BIT_OFFSET 3 /* bit offset of the 'fifo_full' field */
#define STATUS_FIFO_FULL_BIT_WIDTH 1 /* bit width of the 'fifo_full' field */
#define STATUS_FIFO_FULL_BIT_MASK 0x00000008 /* bit mask of the 'fifo_full' field */
#define STATUS_FIFO_FULL_RESET 0x0 /* reset value of the 'fifo_full' field */

/* Register 'reset' */
#define RESET_OFFSET 0x00000008 /* address offset of the 'reset' register */

/* Field  'reset.cam_rst_n' */
#define RESET_CAM_RST_N_BIT_OFFSET 0 /* bit offset of the 'cam_rst_n' field */
#define RESET_CAM_RST_N_BIT_WIDTH 1 /* bit width of the 'cam_rst_n' field */
#define RESET_CAM_RST_N_BIT_MASK 0x00000001 /* bit mask of the 'cam_rst_n' field */
#define RESET_CAM_RST_N_RESET 0x0 /* reset value of the 'cam_rst_n' field */

/* Register 'leds' */
#define LEDS_OFFSET 0x0000000C /* address offset of the 'leds' register */

/* Field  'leds.led0' */
#define LEDS_LED0_BIT_OFFSET 0 /* bit offset of the 'led0' field */
#define LEDS_LED0_BIT_WIDTH 3 /* bit width of the 'led0' field */
#define LEDS_LED0_BIT_MASK 0x00000007 /* bit mask of the 'led0' field */
#define LEDS_LED0_RESET 0x7 /* reset value of the 'led0' field */

#endif  /* CTRL_H */
