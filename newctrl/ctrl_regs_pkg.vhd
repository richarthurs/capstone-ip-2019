-- -----------------------------------------------------------------------------
-- 'ctrl' Register Definitions
-- Revision: 13
-- -----------------------------------------------------------------------------
-- Generated on 2019-02-13 at 19:12 (UTC) by airhdl version 2019.01.1
-- -----------------------------------------------------------------------------
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
-- POSSIBILITY OF SUCH DAMAGE.
-- -----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package ctrl_regs_pkg is

    -- Type definitions
    type slv1_array_t is array(natural range <>) of std_logic_vector(0 downto 0);
    type slv2_array_t is array(natural range <>) of std_logic_vector(1 downto 0);
    type slv3_array_t is array(natural range <>) of std_logic_vector(2 downto 0);
    type slv4_array_t is array(natural range <>) of std_logic_vector(3 downto 0);
    type slv5_array_t is array(natural range <>) of std_logic_vector(4 downto 0);
    type slv6_array_t is array(natural range <>) of std_logic_vector(5 downto 0);
    type slv7_array_t is array(natural range <>) of std_logic_vector(6 downto 0);
    type slv8_array_t is array(natural range <>) of std_logic_vector(7 downto 0);
    type slv9_array_t is array(natural range <>) of std_logic_vector(8 downto 0);
    type slv10_array_t is array(natural range <>) of std_logic_vector(9 downto 0);
    type slv11_array_t is array(natural range <>) of std_logic_vector(10 downto 0);
    type slv12_array_t is array(natural range <>) of std_logic_vector(11 downto 0);
    type slv13_array_t is array(natural range <>) of std_logic_vector(12 downto 0);
    type slv14_array_t is array(natural range <>) of std_logic_vector(13 downto 0);
    type slv15_array_t is array(natural range <>) of std_logic_vector(14 downto 0);
    type slv16_array_t is array(natural range <>) of std_logic_vector(15 downto 0);
    type slv17_array_t is array(natural range <>) of std_logic_vector(16 downto 0);
    type slv18_array_t is array(natural range <>) of std_logic_vector(17 downto 0);
    type slv19_array_t is array(natural range <>) of std_logic_vector(18 downto 0);
    type slv20_array_t is array(natural range <>) of std_logic_vector(19 downto 0);
    type slv21_array_t is array(natural range <>) of std_logic_vector(20 downto 0);
    type slv22_array_t is array(natural range <>) of std_logic_vector(21 downto 0);
    type slv23_array_t is array(natural range <>) of std_logic_vector(22 downto 0);
    type slv24_array_t is array(natural range <>) of std_logic_vector(23 downto 0);
    type slv25_array_t is array(natural range <>) of std_logic_vector(24 downto 0);
    type slv26_array_t is array(natural range <>) of std_logic_vector(25 downto 0);
    type slv27_array_t is array(natural range <>) of std_logic_vector(26 downto 0);
    type slv28_array_t is array(natural range <>) of std_logic_vector(27 downto 0);
    type slv29_array_t is array(natural range <>) of std_logic_vector(28 downto 0);
    type slv30_array_t is array(natural range <>) of std_logic_vector(29 downto 0);
    type slv31_array_t is array(natural range <>) of std_logic_vector(30 downto 0);
    type slv32_array_t is array(natural range <>) of std_logic_vector(31 downto 0);


    -- Revision number of the 'ctrl' register map
    constant CTRL_REVISION : natural := 13;

    -- Default base address of the 'ctrl' register map 
    constant CTRL_DEFAULT_BASEADDR : unsigned(31 downto 0) := unsigned'(x"00000000");
    
    -- Register 'control'
    constant CONTROL_OFFSET : unsigned(31 downto 0) := unsigned'(x"00000000"); -- address offset of the 'control' register
    constant CONTROL_CAM_TRIGGER_BIT_OFFSET : natural := 0; -- bit offset of the 'cam_trigger' field
    constant CONTROL_CAM_TRIGGER_BIT_WIDTH : natural := 1; -- bit width of the 'cam_trigger' field
    constant CONTROL_CAM_TRIGGER_RESET : std_logic_vector(0 downto 0) := std_logic_vector'("0"); -- reset value of the 'cam_trigger' field
    constant CONTROL_EXPORT_TRIGGER_BIT_OFFSET : natural := 1; -- bit offset of the 'export_trigger' field
    constant CONTROL_EXPORT_TRIGGER_BIT_WIDTH : natural := 1; -- bit width of the 'export_trigger' field
    constant CONTROL_EXPORT_TRIGGER_RESET : std_logic_vector(1 downto 1) := std_logic_vector'("0"); -- reset value of the 'export_trigger' field
    
    -- Register 'status'
    constant STATUS_OFFSET : unsigned(31 downto 0) := unsigned'(x"00000004"); -- address offset of the 'status' register
    constant STATUS_CAM_CAPTURING_BIT_OFFSET : natural := 0; -- bit offset of the 'cam_capturing' field
    constant STATUS_CAM_CAPTURING_BIT_WIDTH : natural := 1; -- bit width of the 'cam_capturing' field
    constant STATUS_CAM_CAPTURING_RESET : std_logic_vector(0 downto 0) := std_logic_vector'("0"); -- reset value of the 'cam_capturing' field
    constant STATUS_EXPORTING_BIT_OFFSET : natural := 1; -- bit offset of the 'exporting' field
    constant STATUS_EXPORTING_BIT_WIDTH : natural := 1; -- bit width of the 'exporting' field
    constant STATUS_EXPORTING_RESET : std_logic_vector(1 downto 1) := std_logic_vector'("0"); -- reset value of the 'exporting' field
    constant STATUS_FIFO_EMPTY_BIT_OFFSET : natural := 2; -- bit offset of the 'fifo_empty' field
    constant STATUS_FIFO_EMPTY_BIT_WIDTH : natural := 1; -- bit width of the 'fifo_empty' field
    constant STATUS_FIFO_EMPTY_RESET : std_logic_vector(2 downto 2) := std_logic_vector'("0"); -- reset value of the 'fifo_empty' field
    constant STATUS_FIFO_FULL_BIT_OFFSET : natural := 3; -- bit offset of the 'fifo_full' field
    constant STATUS_FIFO_FULL_BIT_WIDTH : natural := 1; -- bit width of the 'fifo_full' field
    constant STATUS_FIFO_FULL_RESET : std_logic_vector(3 downto 3) := std_logic_vector'("0"); -- reset value of the 'fifo_full' field
    
    -- Register 'reset'
    constant RESET_OFFSET : unsigned(31 downto 0) := unsigned'(x"00000008"); -- address offset of the 'reset' register
    constant RESET_CAM_RST_N_BIT_OFFSET : natural := 0; -- bit offset of the 'cam_rst_n' field
    constant RESET_CAM_RST_N_BIT_WIDTH : natural := 1; -- bit width of the 'cam_rst_n' field
    constant RESET_CAM_RST_N_RESET : std_logic_vector(0 downto 0) := std_logic_vector'("0"); -- reset value of the 'cam_rst_n' field
    
    -- Register 'leds'
    constant LEDS_OFFSET : unsigned(31 downto 0) := unsigned'(x"0000000C"); -- address offset of the 'leds' register
    constant LEDS_LED0_BIT_OFFSET : natural := 0; -- bit offset of the 'led0' field
    constant LEDS_LED0_BIT_WIDTH : natural := 3; -- bit width of the 'led0' field
    constant LEDS_LED0_RESET : std_logic_vector(2 downto 0) := std_logic_vector'("111"); -- reset value of the 'led0' field

end ctrl_regs_pkg;
