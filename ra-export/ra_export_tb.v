`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/18/2019 05:42:59 PM
// Design Name: 
// Module Name: ra_export_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ra_export_tb();

    // Generated
    reg clk;
    reg export_reset_n;
    reg trigger;
    reg [7:0] data;
    reg fifo_empty;
    
    // Monitor
    wire tx;
    wire busy;
    wire rd; 
    
    // Snoop
    wire uart_active_snoop;
    wire uart_done_snoop;

    // Test variables
    reg rd_request_d;
    reg [3:0] read_requests;

    // Export module instantiation
    ra_export#(
        3   // clks per bit
    )export_module(
        .clk(clk),
        .rst_n(export_reset_n),
        .trigger(trigger),
        .r_data(data),
        .uart_tx_out(tx),
        .busy(busy),
        .rd(rd),
        .fifo_empty(fifo_empty),
        .uart_tx_done_o(uart_done_snoop),
        .uart_tx_active_o(uart_active_snoop)
       );


    // 50 MHz System Clock
    always begin
        clk <= 1'b1;
        #10;
        clk <= ~clk;
        #10;
    end

    // test variables


    // Testing loop
    initial begin
    
    // Init the test variables
    rd_request_d <= 1'b0;
    read_requests <= 4'b0;
    
    // Init the signals
    export_reset_n <= 1'b1;
    trigger <= 1'b0;
    data <= 8'h47;
    fifo_empty <= 1'b0;
    
 
    
    // Wait a little
    repeat(3) @(posedge clk);
    
    // Trigger
    trigger <= 1'b1;
    @(posedge clk);
//    trigger <= 1'b0;
    // should be exporting by now
    
    // Wait for a few read requests to happen
    while(read_requests < 3) begin
        // Count number of rising edges of read signal
        
        if(rd_request_d == 1'b0 && rd == 1'b1) begin
            read_requests = read_requests + 1;
            data = data + 1;
        end 
        
        rd_request_d <= rd;
        @(posedge clk);
    end
    
    // Now that some requests have occurred, assert empty
    fifo_empty <= 1'b1;
    
    // pump out some clocks, everything should stop
    repeat(300) @(posedge clk);
    
    // Turn off the trigger and pump some clocks, everything should drain
    trigger <= 1'b0;
    repeat(300) @(posedge clk);
    
    end

endmodule
