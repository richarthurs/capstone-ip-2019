`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/18/2019 03:49:57 PM
// Design Name: 
// Module Name: ra-export
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
/*

    Set clocks per bit: 10 MHZ and 115200 baud: = 10,000,000 / 115200 = 87
*/
//////////////////////////////////////////////////////////////////////////////////

`default_nettype wire
module ra_export#(
        parameter CLKS_PER_BIT = 87
    )(
    input clk,
    input rst_n,
    input trigger,
    input [7:0] r_data,
    output wire uart_tx_out,
    output reg busy,
    output reg rd,
    input fifo_empty,
    output wire uart_tx_done_o,     // snoop output from embedded uart
    output wire uart_tx_active_o    // snoop output from embedded uart 
    );
    
    localparam IDLE = 3'b000;
    localparam REQUEST = 3'b001;
    localparam SETUP_UART = 3'b010;
    localparam WAIT_FOR_TX = 3'b011;
    
    reg [2:0] state = IDLE;
    reg trigger_d;  // previous trigger
    
    reg tx_start;           // trigger for UART
    wire uart_tx_active;
    wire uart_tx_done;    
    
    // Snoop assignments
    assign uart_tx_done_o = uart_tx_done;
    assign uart_tx_active_o = uart_tx_active;
    
    
    
    // Instantiate the UART TX
    uart_tx#(CLKS_PER_BIT) 
        m_uart(
        .i_Clock(clk),
        .i_Tx_DV(tx_start),
        .i_Tx_Byte(r_data),     
        .o_Tx_Active(uart_tx_active),
        .o_Tx_Serial(uart_tx_out),
        .o_Tx_Done(uart_tx_done)
    );
    
    initial begin
        busy <= 1'b0;
        rd <= 1'b0;
        tx_start <= 1'b0;
    end
    
    
    always @(posedge clk) begin
        case(state)
            IDLE: begin
                busy <= 1'b0;       // Not busy
                rd <= 1'b0;         // Don't read
                tx_start <= 1'b0;   // Don't transmit
                
                // Rising edge of trigger and UART is idle, let's go
                if(trigger == 1'b1 && trigger_d == 1'b0 && uart_tx_active == 1'b0) state <= REQUEST;
                
                // Update trigger
                trigger_d <= trigger;
            end
            
            REQUEST: begin
                busy <= 1'b1;               // Busy
                rd <= 1'b1;                 // Request data
                state <= SETUP_UART;
            end
            
            SETUP_UART: begin
                if(~fifo_empty && ~uart_tx_active) begin
                    // Valid data is ready to go
                    rd <= 1'b0;             // No more data please
                    tx_start <= 1'b1;       // Kick off the transfer
                    // data is already presented to the UART with the direct connection
                    
                    state <= WAIT_FOR_TX;
                end
                else begin
                    // FIFO is empty, go back to IDLE
                    state <= IDLE;
                end
            end
            
            WAIT_FOR_TX: begin
                tx_start <= 1'b0;
                
                // Wait around for the transmission to finish, then request a new byte
                // TODO: we can queue up the next byte and save some cycles
                if(uart_tx_active == 1'b1 && uart_tx_done == 1'b0) state <= WAIT_FOR_TX;
                else if(uart_tx_active == 1'b0 && uart_tx_done == 1'b1) state <= REQUEST;
//                else state <= IDLE;
            end
          
        endcase
    end    
endmodule
