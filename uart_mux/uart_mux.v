`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Richard Arthurs
// 
// Create Date: 07/22/2019 03:11:35 PM
// Design Name: 
// Module Name: uart_mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
/*

This module allows the piping of data to the UARTs to be controlled. 

The export_axi_tx_mux register is used by FW to control whether the AXI UART has control over the TX pin, or whether the export module does. 
    - During an export, FW will flip it to export module control. (1)
    - Normally, it is set to AXI UART control.  (0, default)

The tx_ext_jtag_uart_mux and rx_ext_jtag_uart_mux are used by FW to select whether the JTAG UART pins or the FTDI uart pins are used. 
    - Normally, we use the FTDI for both. (0, default)
    - It can be useful to hook up the JTAG UART instead, flip these bits to 1 to select this mode. 
    
*/
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_mux(
    // Multiplex selection for RX/TX
    input wire tx_ext_jtag_uart_mux,
    input wire rx_ext_jtag_uart_mux,

    // IOs
    input wire jtag_rx,
    output wire jtag_tx,
    input wire ftdi_rx, 
    output wire ftdi_tx,
    
    
    // AXI vs. Export control of UART TX
    input wire export_axi_tx_mux, // 0 = AXI TX is connected to board TX, 1 = Export TX is connected to board TX
    input wire axi_tx,
    input wire export_tx,    
    
    // Module outputs
    output wire rx 
    );
    
    wire internal_tx;
    
    // Assign for TX 
    assign internal_tx = (export_axi_tx_mux) ? export_tx : axi_tx;
        
    // Assign the RX and TX - 0 = FTDI, 1 = JTAG
    assign rx = (rx_ext_jtag_uart_mux) ? jtag_rx : ftdi_rx;
    assign ftdi_tx = (tx_ext_jtag_uart_mux) ? 1'bz : internal_tx;
    assign jtag_tx = (tx_ext_jtag_uart_mux) ? internal_tx : 1'bz;

    
endmodule
