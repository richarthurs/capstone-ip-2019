`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Richard Arthurs
// 
// Create Date: 01/21/2019 05:28:22 PM
// Design Name: 
// Module Name: camera_if
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module camera_if#(
    POST_FRAME_PCLK_THRESH = 500,   
    DATA_WIDTH = 10     // Camera bit depth
    )(
    input wire clk,
    input wire pclk,
    input wire [DATA_WIDTH-1:0] data,
    input wire vsync,
    input wire href,
    input wire trigger,
    output reg [DATA_WIDTH-1:0] data_out,
    output reg valid, 
    output wire pclk_out,
    output reg busy_out,
    output reg[31:0] pxcount_out,
    output reg valid_100_safe_out,
    output reg href_100_safe_out, 
    output reg vsync_100_safe_out,
    output reg pclk_100_safe_out
    );
    
    
    // Capture State Machine
    // idle
        // if trigger is 1 - wait for vsync
    // wait for vsync
        // wait for vsync pulse to go high. Then go to wait for href
   // pre frame
       // wait for href to go high, then go to line
       // register the first pixel 
   // Wait for href
       // when href goes high, capture the first pixel and go to ROW
       // count the number of clocks, if it is more than 322, go to idle
  // line
      // every clock, register the new image data, if href goes low, go to wait for href
      
    reg[2:0] capture_state;
    parameter IDLE = 3'b000;
    parameter WAIT_VSYNC = 3'b001;
    parameter PRE_FRAME = 3'b010;
    parameter WAIT_HREF = 3'b011;
    parameter LINE = 3'b100;
    
    // Capture Variables
    reg trigger_safe;           // Synchronized to pclk domain
    reg trigger_metastable;     // Potentially metastable
    reg busy;                   // Pclk domain
    reg busy_metastable;        // Potentially metastable
    reg [31:0] pxcount_metastable;  // Potentially metastable
    reg [31:0] pxcount;
    reg valid_100_metastable;
    reg href_100_metastable;
    reg vsync_100_metastable;
    reg pclk_100_metastable;
    
    reg vsync_d;
    reg href_d;
    reg[15:0] href_clk_count;    // Must be able to count to 322

    assign pclk_out = pclk;
  
    // Initialization
    initial begin
        capture_state <= IDLE;
        vsync_d <= 1'b0;
        href_d <= 1'b0;
        href_clk_count <= 16'b0;
        valid <= 1'b0;
        busy <= 1'b0;
        pxcount <= 'b0;
        pxcount_out <= 'b0;
        pxcount_metastable <= 'b0;
     end
     
     
    // Trigger Synchronizer - sync from clk to pclk domain
    // Since trigger stays high, don't need to worry about the fast -> slow domain transition (I think)
    always @(posedge pclk)
        {trigger_safe, trigger_metastable} <= {trigger_metastable, trigger};
     
    // Busy Synchronizer - sync from pclk to clk domain
    always @(posedge clk)
        {busy_out, busy_metastable} <= {busy_metastable, busy}; 
        
    // Pxclk synchronizer - sync from pclk to clk domain
    always @(posedge clk)
        {pxcount_out, pxcount_metastable} <= {pxcount_metastable, pxcount};
        
    // Valid synchronizer - for debugging
    // Normal valid is still in pclk domain because it feeds only the async FIFO
    // this valid goes to ILA, so is flopped into the fast domain
    always @(posedge clk)
        {valid_100_safe_out, valid_100_metastable} <= {valid_100_metastable, valid};
        
    // Synchronize debug href
    always @(posedge clk)
        {href_100_safe_out, href_100_metastable} <= {href_100_metastable, href};
     
    // Synchronize debug vsync
    always @(posedge clk)
        {valid_100_safe_out, valid_100_metastable} <= {valid_100_metastable, valid};
        
    // Synchronize debug pclk
    always @(posedge clk)
        {pclk_100_safe_out, pclk_100_metastable} <= {pclk_100_metastable, pclk};
     
    // Capture State Machine
    always @(posedge pclk) begin
    case(capture_state)
        IDLE: begin
            if(trigger_safe == 1'b1) capture_state <= WAIT_VSYNC;
            else capture_state <= IDLE;
            
            vsync_d <= vsync;
            valid <= 1'b0;
            busy <= 1'b0;
        end
        
        WAIT_VSYNC: begin
            if(vsync_d == 1'b0 && vsync == 1'b1) capture_state <= PRE_FRAME;    // Capture the rising edge of vsync
            else capture_state <= WAIT_VSYNC;
            vsync_d <= vsync;
            href_d <= href;
            busy <= 1'b1;
        end
        
        PRE_FRAME: begin
            if(href_d == 1'b0 && href == 1'b1) begin
                // Register First Pixel
                valid <= 1'b1;
                data_out[DATA_WIDTH-1:0] <= data[DATA_WIDTH-1:0];
                capture_state <= LINE;
                pxcount <= 'h00000001;
            end
            else capture_state <= PRE_FRAME;
            
            href_d <= href;
        end
        
        WAIT_HREF: begin
            // When the href low times are more than 322 pclks, it is the end of the frame
            href_clk_count <= href_clk_count + 1;
            
            if(href_d == 1'b0 && href == 1'b1 && href_clk_count < POST_FRAME_PCLK_THRESH) begin    
                // register first pixel
                valid <= 1'b1;
                data_out[DATA_WIDTH-1:0] <= data[DATA_WIDTH-1:0];
                    
                capture_state <= LINE;
                href_clk_count <= 16'b0;
            end
            else if(href_clk_count >= POST_FRAME_PCLK_THRESH) begin // end of the frame, go to IDLE
                capture_state <= IDLE;
                href_clk_count <= 16'b0;
            end
            else capture_state <= WAIT_HREF;
            
            href_d <= href;
        end
        
        LINE: begin
            if(href == 1'b1) begin
              // Register the Pixels
              // TODO: might need to count these since it may assert valid for a false clock
                valid <= 1'b1;
                data_out[DATA_WIDTH-1:0] <= data[DATA_WIDTH-1:0];
                pxcount <= pxcount + 1;
            end
            
            // End of line, HREF low 
            else if(href == 1'b0) begin 
                valid <= 1'b0;
                data_out[DATA_WIDTH-1:0] <= 'b0;
                href_d <= 1'b0;
                href_clk_count <= 16'b0;
                capture_state <= WAIT_HREF;
           end
        end
        
        default: capture_state <= IDLE;
    endcase
    end
endmodule
