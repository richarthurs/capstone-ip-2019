`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/19/2019 09:42:29 AM
// Design Name: 
// Module Name: cam_mock_if_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module cam_mock_if_tb(

    );
    
    // DUT inputs
    reg clk;
    reg rst_n;
    reg trigger;
    
    // DUT module outputs
    wire [9:0] image;
    wire href;
    wire vsync;
    
    // DUT outputs - camera IF
    wire valid;
    wire [9:0] data_out;
    wire busy;
    
    // Instantiate the mock camera   
    camera_mock#(
        // Set up the image size: 16x16
        .ROWS(12'd16),
        .COLS(12'd16),
        
        // Shorten the delays for simulation
       .VSYNC_PULSE_CLKS(15'h05),
        .PRE_FRAME_CLKS(15'd10),
        .NEW_ROW_CLKS(15'h04),        
        .POST_FRAME_CLKS(15'h10)    // 16   
    ) cam_inst(
        .clk(clk),
        .rst_n(rst_n),
        .image(image),
        .href(href),
        .vsync(vsync)
    );
    
    
    // Camera Interface Instantiation
    camera_if#(
        10  // After this many pclks with href low, we will go idle again
    ) cam_if_inst(
        .clk(clk),
        .pclk(clk),
        .data(image),
        .href(href),
        .vsync(vsync),
        .valid(valid),
        .trigger(trigger),
        .data_out(data_out),     
        .busy_out(busy)
    );
    
    // Generate clock signal
    always begin
        clk <= 1'b1;
        #1;
        clk <= ~clk;
        #1;
    end
    
    initial begin
    // Init 
    rst_n <= 1'b0;
    trigger <= 1'b0;
    @(posedge clk);
    @(posedge clk);
    
    // Enable everything
    rst_n <= 1'b1;
    @(posedge clk);
    @(posedge clk);

    // Toggle the trigger
    trigger <= 1'b1;
    @(posedge clk);
    @(posedge clk);
    trigger <= 1'b0;

    
    end
endmodule
