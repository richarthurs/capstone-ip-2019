`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Richard Arthurs
// 
// Create Date: 06/08/2019 11:38:39 AM
// Design Name: 
// Module Name: hwrap
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: wrapper for HWRAP_core (our abstraction of HRAM behaviour), and the HRAM interface 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hwrap(
    input wire clk,
    
    // AXI Reg Connections
    input wire rst_n,
    input wire [1:0] hwrap_mode,        // Mode setter
    output wire hwrap_idle,             // Idle status of wrapper
    input wire mode_10_bit,             // 8/10 bit mode setter
    output wire [31:0]  hram_ptr,       // Current read/write address of HRAM wrapper
    
    // HWRAP Core Connections
    input wire [9:0] in_data,           // Input write data - connect to BUF
    input wire in_wr_req,               // valid asserting that BUF has data to feed to HRAM
    output wire read_from_input_buf,    // HWRAP requests read from input buffer, active high
    
    // HRAM 
    output wire hram_clk,
    output wire hram_rst_n,
    output wire hram_cs_n,
    inout wire [7:0] hram_data,  
    inout wire hram_rwds,
    output wire [7:0] hram_dbg,
    
    // Debug
    output wire hram_wr_req_n_dbg,
    output wire state_out
  );
  
  // Ties
  reg mem_or_reg_tie;
  reg [3:0] wr_byte_en_tie;
  reg [5:0] rd_num_dwords_tie;
  reg rd_req_n_tie; // rd_req_n is getting tied to 0 somehow coming out of Hwrap core, this causes hram_xface to have a read request since it's active low .... 
  
  initial begin
    mem_or_reg_tie <= 1'b1;             // 1 = DRAM, 0 = reg
    wr_byte_en_tie  <= 4'hF;            // 0xF = Write all 4 bytes. 0xE write Bytes 3-1 but not 0.
    rd_num_dwords_tie <= 6'b000001;     // Number of dwords to read, example 0x01.
    rd_req_n_tie <= 1'b1;
  end
  
  // Wires to connect modules
  wire hram_idle;           // Complement of HRAM's busy signal
  wire hram_wr_req_n;       // Write request to HRAM from HWRAP
  wire hram_rd_req_n;       // Read request to HRAM from HWRAP
  wire [31:0] hram_addr;    // HRAM read/write address from HWRAP
  wire [31:0] hram_wr_data; // HRAM write data from HWRAP
  wire [31:0] hram_rd_data; // HRAM read data from HWRAP
  wire hram_rd_ready;       // HRAM read data is valid
  
  // Wires for bidirectional signals
  wire [7:0]   dram_dq_in;
  wire [7:0]   dram_dq_out;
  wire dram_dq_oe_l;
  wire dram_rwds_in;
  wire dram_rwds_out;
  wire dram_rwds_oe_l;
  
  // Wires for polarity flipping
  wire hram_busy;
  assign hram_idle = ~hram_busy;
  
  // Bidirectional assigns
  assign hram_data = dram_dq_oe_l ? 8'bz : dram_dq_out;     // oe_l goes to 1 for reads - when the module needs an input
  assign dram_dq_in = hram_data;
  assign hram_rwds = dram_rwds_oe_l ? 1'bz : dram_rwds_out;
  assign dram_rwds_in = hram_rwds; 
  
  // Debug assigns
  assign hram_wr_req_n_dbg = hram_wr_req_n;
  
  // Hack to get HRAM xface to behave - tie 
  
    // HRAM Core Instantiation
    hwrap_core#() hwrap_inst(
        .clk(clk),
        .rst_n(rst_n),
        
        .hwrap_mode(hwrap_mode),        // AXI 
        .hwrap_idle(hwrap_idle),        // AXI
        .in_wr_data(in_data),           
        .mode_10_bit(mode_10_bit),      // AXI
        .in_wr_req(in_wr_req),
        .in_buf_rd_req(read_from_input_buf),
        .hram_ptr(hram_ptr),            // AXI
               
       // Connections to HRAM
       .hram_idle(hram_idle),               // input from HRAM
       .hram_addr(hram_addr),               // output to hram, HRAM address to write/read
       .hram_wr_data(hram_wr_data),         // output to hram, hram write data input
       .hram_wr_req_n(hram_wr_req_n),       // output to hram, active low hram write request
       .hram_rd_data(hram_rd_data),         // input from HRAM, read data
       .hram_rd_rdy(hram_rd_ready),         // input from HRAM, read data is valid
       .hram_rd_req_n(hram_rd_req_n),        // output to HRAM, read request is present
       
       // Debug
       .state_out(state_out)
    );
    
     
    // HRAM Interface Instantiation
    hyper_xface#() hram_if(
        .clk(clk),
        .reset(~rst_n),      // Xface is active high reset

        .rd_req(rd_req_n_tie), // TODO: normally this should wire into hram_rd_req_n output of core through wire of the same name. Synth is tying it to 0 for some reason. 
        .wr_req(hram_wr_req_n),
        .mem_or_reg(mem_or_reg_tie),
        .wr_byte_en(wr_byte_en_tie),
        .rd_num_dwords(rd_num_dwords_tie),
        .addr(hram_addr),
        .wr_d(hram_wr_data),
        .rd_d(hram_rd_data),
        .rd_rdy(hram_rd_ready),
        .busy(hram_busy),  // HWRAP uses IDLE terminology instead of busy TODO: will this invert work if it's driven by hram xface?
//        output reg          burst_wr_rdy, // RA: not required
        // TODO: is making these zero ok?
//        input  wire [7:0]   latency_1x,
//        input  wire [7:0]   latency_2x,
      
        .dram_dq_in(dram_dq_in),
        .dram_dq_out(dram_dq_out),
        .dram_dq_oe_l(dram_dq_oe_l),
      
        .dram_rwds_in(dram_rwds_in),
        .dram_rwds_out(dram_rwds_out),
        .dram_rwds_oe_l(dram_rwds_oe_l),
      
        .dram_ck(hram_clk),
        .dram_rst_l(hram_rst_n),
        .dram_cs_l(hram_cs_n),
        .sump_dbg(hram_dbg)
    
    );



  
endmodule
