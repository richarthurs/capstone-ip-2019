module hwrap_rd_tb(
    );
    
    /* Hyperram core read testbench 
        - issues a bunch of reads to HRAM core
        - models HRAM with a short delay 
    
    */
    
    // DUT inputs
    reg clk;
    reg rst_n;
    reg [1:0] hwrap_mode;
    reg mode_10_bit;
    reg in_rd_req;
    reg [31:0] in_rd_addr;
    
    
    // DUT Outputs
    wire hwrap_idle;
    wire rd_data_valid;
    wire [31:0] rd_data;
    wire hram_rd_req_n;
    
    
    // Mocked HRAM
    reg hram_idle;  // input to DUT
    reg [31:0] mock_rd_data;    // input to DUT
    reg hram_rd_rdy;            // input to DUT
    
    // Simulation Variables
    reg[4:0] num_requests;
    
     
     // HWRAP Instantiation 
     hwrap_core#() hwrap_inst(
         .clk(clk),
         .rst_n(rst_n),
         .hwrap_mode(hwrap_mode),
         .hwrap_idle(hwrap_idle),
         .mode_10_bit(mode_10_bit),
         .rd_req(in_rd_req),
         .rd_addr(in_rd_addr),
         .rd_data_valid(rd_data_valid),
         .rd_data(rd_data),
         
         .hram_idle(hram_idle),
         .hram_rd_data(mock_rd_data),
         .hram_rd_rdy(hram_rd_rdy),
         .hram_rd_req_n(hram_rd_req_n)
     );
     
     
    // 100 MHz System Clock
    always begin
        clk <= 1'b1;
        #5;
        clk <= ~clk;
        #5;
    end
     
     
     // Simulation LOOP
     initial begin
        // Set up simulation vars
        mock_rd_data <= 'hDEADBEEF;
        num_requests <= 'b0;

        
          // Setup HW
          hwrap_mode <= 2'b01;    // write mode
          rst_n <= 1'b1;          // active
          mode_10_bit <= 1'b0;    // 8 bit mode
          hram_idle <= 1'b1;      // RAM is idle
          in_rd_req <= 1'b0;      // No read request
          hram_rd_rdy <= 1'b0;
          in_rd_addr <= 'b0;

          
          // Reset to latch all of the configuration
          @(posedge clk);
          rst_n <= 1'b0;
          @(posedge clk);
          rst_n <= 1'b1;
          @(posedge clk);
          
          // Start threads
          fork 
            mock_hram();
            submit_request();
          join
          
     end
     
     task submit_request();
        begin
             // Wait a little while
             repeat(3) @(posedge clk);
             
             while(num_requests < 3) begin
                // Submit a request
               in_rd_req <= 1'b1;
               @(posedge clk);
               in_rd_req <= 1'b0;
               @(posedge clk);
               
               // Wait for the request to finish
               while(!rd_data_valid) begin
                    @(posedge clk);
               end
               
               num_requests <= num_requests + 1;
               in_rd_addr <= in_rd_addr + 4;
             end

             repeat(50) @(posedge clk);
        end
     endtask
     
     // Mock the HRAM response - take a few cycles before asserting that the read is done
     task mock_hram();
        begin
        while(num_requests < 3) begin
            while(hram_rd_req_n == 1'b1) begin  // Wait while request is not required
                @(posedge clk); 
                hram_rd_rdy <= 1'b0;
                hram_idle <= 1'b1;
            end   
            hram_idle <= 1'b0;
            repeat(10) @(posedge clk);          // HRAM is reading, takes a while
            hram_idle <= 1'b1;
            hram_rd_rdy <= 1'b1;
            @(posedge clk);
            mock_rd_data <= mock_rd_data + 2332;
            
        end
        end
     endtask
    
endmodule