`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/01/2019 03:51:04 PM
// Design Name: 
// Module Name: hwrap
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hwrap_core(
    input wire clk,                 // Main clk - this is fed to HRAM and then divided by 4 to create DDR signals
    input wire rst_n,               // Synchronous active low reset
    
    // Common
    input wire [1:0] hwrap_mode,    // b00 = WRITE, b01 = READ, b10 = FLUSH
    input wire mode_10_bit,         // 1 = 10-bit, 0 = 8-bit
    output reg hwrap_idle,          // Idle flag for HRAM module - encompasses HRAM idle and module idle

    // Write
    input wire [9:0] in_wr_data,    // Data to write - 8 or 10 bit supported based on mode_10_bit
    input wire in_wr_req,           // Host asserts this to write. Supports burst.  
    output reg in_buf_rd_req,       // During write, this requests data from the connected input buf
    output wire [31:0] hram_ptr,    // Current HRAM read or write pointer - connect to AXI
    
    // Read
    input wire rd_req,              // Read request from host, active high
    input wire [31:0] rd_addr,      // Desired read address 
    output reg rd_data_valid,       // Read data from HRAM is good - asserts 1 clk
    output reg [31:0] rd_data,      // Read data from HRAM
    
    
    // Connections to HRAM
    input wire hram_idle,              // input from HRAM
    output reg hram_wr_req_n,          // output, active low hram write request
    output reg [31:0] hram_wr_data,    // output, hram write data input
    output reg [31:0] hram_addr,       // output, HRAM address to write in
    output reg hram_rd_req_n,            
    input wire [31:0] hram_rd_data,
    input wire hram_rd_rdy,
    
    output wire [2:0] state_out    
    );  
    
// Params/flags
parameter WRITE_MODE = 2'b00;
parameter READ_MODE = 2'b01;
parameter FLUSH_MODE = 2'b10;

// Module status 
parameter IDLE = 1'b1;
parameter BUSY = 1'b0;

// Internal registers - common mode
//reg [1:0] module_mode;      // b00 = WRITE, b01 = READ, b10 = FLUSH 
//reg is_10_bit;              // 0 = 8-bit mode, 1 = 10-bit mode
reg [2:0] state;            // State within mode


// Internal registers/params - WRITE mode
parameter ALL_IDLE = 3'b000;            // IDLE valid in any mode - mode state machines will move this around
parameter W_REG_WR_DATA = 3'b001;       // Register data to write
parameter W_REQ_HRAM_WRITE = 3'b010;    // Request for data to be written
parameter W_WAIT_HRAM_WRITE = 3'b011;   // Wait for write to complete

reg [3:0] dword_buf[7:0];               // Internal dword buffer that is eventually written to HRAM
reg [1:0] current_dword;                // DWORD index of the internal dword_buf

// Internal registers/params - READ mode
parameter R_REQ_WAIT = 3'b100;          // Wait for read request to complete

// Internal regs
reg [31:0] hram_addr_w; // Address flops for the different states - to avoid multi driven net
reg [31:0] hram_addr_r;
reg [31:0] hram_addr_fl; 

// Initial - default values 
initial begin
    hwrap_idle <= 1'b1;
    in_buf_rd_req <= 1'b0;
    hram_wr_req_n <= 1'b1;
    hram_wr_data <= 'b0;
    hram_addr_w <= 'b0;
    hram_addr_r <= 'b0;
    hram_addr_fl <= 'b0;

//    hram_addr <= 'b0;
    
    // READ
    rd_data_valid <= 1'b0;
    rd_data <= 'b0;
    hram_rd_req_n <= 1'b1;
end

always @(posedge clk) begin

    // RESET handler
    if(rst_n == 1'b0) begin 
        // Register the mode settings so they can't be changed during a capture
        // RA: removed because it causes multi driver issues
//        module_mode <= hwrap_mode;
//        is_10_bit <= mode_10_bit;


        state <= ALL_IDLE;          // Stay idle

//        hram_addr <= 'b0;
        
        // Initialize everything else
//        in_buf_rd_req <= 1'b0;      // No read request
        hram_wr_req_n <= 1'b1;      // No HRAM write request
        hwrap_idle <= 1'b1;
        current_dword <= 2'b0;
        hram_addr_w <= 'b0;

        
        // READ
        rd_data_valid <= 1'b0; 
        rd_data <= 'b0;
        hram_rd_req_n <= 1'b1;
    end

    if(rst_n == 1'b1) begin
    // WRITE_MODE State Machine
        if(hwrap_mode == WRITE_MODE) begin
            case(state) 
                ALL_IDLE: begin
                    if(in_wr_req) begin                 // we have data to write
                        in_buf_rd_req <= 1'b1;          // Request a read from the attached buffer
                        hwrap_idle <= 1'b0;             // Not idle any more
                        state <= W_REG_WR_DATA;
                    end 
                    else begin
                        hwrap_idle <= 1'b1;             // This block is idle
                        in_buf_rd_req <= 1'b0;          
                        state <= ALL_IDLE;
                    end
                    
                    hram_wr_req_n <= 1'b1;          // no HRAM write request
                    hram_rd_req_n <= 1'b1;
                end
                
                W_REG_WR_DATA: begin                        // Register the data
                    // Issue another request if data is still available
    //                if(in_wr_req && current_dword < 3) begin
    //                    in_buf_rd_req <= 1'b1;                  
    //                end else begin
    //                    in_buf_rd_req <= 1'b0;
    //                end
                    
                    // Handle buffering data into the dword buf
                    if(!mode_10_bit) begin                        // 8-bit mode
                        dword_buf[current_dword] <=  in_wr_data[7:0];
                        
                        if(current_dword < 3) begin             // Not done filling the buffer yet
                            current_dword <= current_dword + 1;
                            if(in_wr_req) begin
                                state <= W_REG_WR_DATA;         // Stay in this state and grab the next pixel
                                in_buf_rd_req <= 1'b1;
                             end else begin
                                state <= ALL_IDLE;              // No pending req - go wait for another request
                                in_buf_rd_req <= 1'b0;
                             end
                        end else begin
                            current_dword <= 'b0;    
                            in_buf_rd_req <= 1'b0;                    
                            state <= W_REQ_HRAM_WRITE;              // Go execute the write
                        end
                        
                    end
                    else begin
                        // TODO: handle 10 bit yuck
                    end
                
                end
                
                W_REQ_HRAM_WRITE: begin     // Trigger the write request
                    if(hram_idle) begin     // HRAM idle, we can init the request
                        hram_wr_req_n <= 1'b0;
                        hram_wr_data <= dword_buf[0] | (dword_buf[1] << 8) | (dword_buf[2] << 16) | (dword_buf[3] << 24); 
                        // TODO: set the HRAM address to hram_addr
                        state <= W_WAIT_HRAM_WRITE;
                    end else begin
                    // wait for HRAM to free up 
                        state <= W_REQ_HRAM_WRITE;
                    end
                end
                
                W_WAIT_HRAM_WRITE: begin
                    hram_wr_req_n <= 1'b1;
                    state <= ALL_IDLE; 
                    hram_addr_w <= hram_addr_w + 4; // add 4 because we just successfully wrote a DWORD and it's a byte address
                end
            endcase 
        end // WRITE_MODE
    
    // READ_MODE State Machine
        if(hwrap_mode == READ_MODE) begin
           
            case(state) 
                ALL_IDLE: begin
                    hwrap_idle <= 1'b1;
    
                    if(hram_idle == 1'b1 && rd_req == 1'b1) begin   // Have a request that we can service
                        hram_rd_req_n <= 1'b0;
                        rd_data_valid <= 1'b0;
                        hwrap_idle <= 1'b0;
                        hram_addr_r <= rd_addr;
                        state <= R_REQ_WAIT;
                    end
                    else begin
                         state <= ALL_IDLE;
                    end
               end
               R_REQ_WAIT: begin
                    hram_rd_req_n <= 1'b1;
                    
                    if(hram_rd_rdy == 1'b1) begin                   // When HRAM says the request is ready
                        rd_data_valid <= 1'b1;
                        hwrap_idle <= 1'b1;
                        rd_data <= hram_rd_data;
                        state <= ALL_IDLE;
                    end
                    else begin                                      // Wait for data to become valid
                        state <= R_REQ_WAIT;
                    end
               end
            endcase
            
            // Reset handler
            if(rst_n == 1'b0) begin 
                hram_addr_r <= 'b0;
            end
        end
    end
end

// IO Flops
always @(posedge clk) begin
    
    // HRAM addr assignment based on the current state
    case(hwrap_mode)
        WRITE_MODE: hram_addr <= hram_addr_w;
        READ_MODE:  hram_addr <= hram_addr_r;
        FLUSH_MODE: hram_addr <= hram_addr_fl;
    endcase
end


// IO Assignments
assign hram_ptr = hram_addr;
assign state_out = state;
    
    
endmodule
