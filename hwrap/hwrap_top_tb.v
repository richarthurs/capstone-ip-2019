`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/21/2019 09:28:45 AM
// Design Name: 
// Module Name: hwrap_top_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hwrap_top_tb();

    // DUT inputs
    reg clk;
    reg rst_n;
    reg [1:0] hwrap_mode;
    reg [9:0] in_data;
    reg mode_10_bit;
    reg in_wr_req;
    
    
    // DUT Outputs
    wire hwrap_idle;
    wire read_from_input_buf;
    wire [31:0] hram_ptr;
    wire [31:0] hram_wr_data;
    wire [31:0] hram_sent_addr;
    
    // DUT debug outputs
    wire hram_wr_req_n_dbg;
    
    // Low-level HRAM IF outputs
    wire hram_clk;
    wire hram_rst_n;
    wire [7:0] hram_data;
    wire hram_rwds;
    wire [7:0] hram_dbg;
    
    hwrap#() hwrap_inst(
        .clk(clk),
        
        // AXI Reg Connections
        .rst_n(rst_n),
        .hwrap_mode(hwrap_mode),        // Mode setter
        .hwrap_idle(hwrap_idle),             // Idle status of wrapper
        .mode_10_bit(mode_10_bit),             // 8/10 bit mode setter
        .hram_ptr(hram_ptr),       // Current read/write address of HRAM wrapper
        
        // HWRAP Core Connections
        .in_data(in_data),           // Input write data - connect to BUF
        .in_wr_req(in_wr_req),               // valid asserting that BUF has data to feed to HRAM
        .read_from_input_buf(read_from_input_buf),    // HWRAP requests read from input buffer, active high
        
        // HRAM 
        .hram_clk(hram_clk),
        .hram_rst_n(hram_rst_n),
        .hram_cs_n(hram_cs_n),
        .hram_data(hram_data),  
        .hram_rwds(hram_rwds),
        .hram_dbg(hram_dbg),
        
        // Debug
        .hram_wr_req_n_dbg(hram_wr_req_n_dbg)
    );
    

     // Simulation Variables
     
     // 100 MHz System Clock
     always begin
         clk <= 1'b1;
         #5;
         clk <= ~clk;
         #5;
     end
    
    
    // TESTING LOOP
    initial begin
    
    // Setup simulation variables

    

    // Reset to latch all of the configuration
    @(posedge clk);
    rst_n <= 1'b0;
    @(posedge clk);
    rst_n <= 1'b1;
    @(posedge clk);
    
        // Setup HW
    hwrap_mode <= 2'b00;    // write mode
    in_data <= 10'h001;     // set up some input data
    rst_n <= 1'b1;          // active
    mode_10_bit <= 1'b0;    // 8 bit mode
    in_wr_req <= 1'b0;      // No write request
    
    
    // Wait a little while
    repeat(3) @(posedge clk);

/// VERIFY INITIAL STAGES OF REQUESTS WITH DIFFERENT DELAYS

    // Initiate the request
    in_wr_req <= 1'b1;      // make the request
    // Drain
    repeat(500) @(posedge clk);   


end
//    // Wait for the request to HRAM to execute
//    while(hram_wr_req_n == 1'b1) begin
//        @(posedge clk);
//    end
//    in_wr_req <= 1'b0;          // no more request

//    // Fake the HRAM response
//    hram_idle <= 1'b0;  // RAM is active for a bit
//    repeat(5) @(posedge clk);   // wait around for the write to complete
//    hram_idle <= 1'b1;

//    // Drain
//    repeat(20) @(posedge clk);   // wait around for the write to complete
   
   
//// VERIFY REPEATED REQUESTS - Burst operation
//    in_data <= 10'h05;
    
//    in_wr_req <= 1'b1;  // make the request
        
//    fork
//        monitor();          // Count outstanding requests
//        data_increment();   // Handle incremeting the write data
//        hram_response();    // Simulate the HRAM response
//        control_input();    // Deassert write request after a certain number of requests
//    join

//    // Drain
//    repeat(20) @(posedge clk);   
    
//    // Reset to check that everything zeros out
//    @(posedge clk);
//    rst_n <= 1'b0;
//    @(posedge clk);
//    rst_n <= 1'b1;
//    @(posedge clk);
    
//    // Drain
//    repeat(20) @(posedge clk);   

//    end    // initial begin
    
    
//    // Control input task - hold write request high for a number of pixels
//    task control_input;
//        begin
//            while( count_pixels < 20) begin
//                in_wr_req <= 1'b1;
//                @(posedge clk);
//            end
//            in_wr_req <= 1'b0;
//        end
//    endtask
    
    
//    // Data increment task  - increments the write data in a predictable pattern so it can be examined
//    task data_increment;
//        begin
//            while(run_tasks) begin
            
//             // Wait for the read request to BUF to come through
//             while(read_from_input_buf == 1'b0) begin  // Wait for the read request to go high 
//                 @(posedge clk);
//             end
             
//             // Wait for 4x pixels to be read
//             while(read_from_input_buf == 1'b1) begin
//                 count_pixels <= count_pixels + 1;
//                 if(in_data == 'h08) in_data <= 'h01;
//                 else in_data <= in_data + 1;
//                 @(posedge clk);
//             end
            
//            end
//        end
//    endtask
    
    
//    // HRAM Response task - fake the response from the HRAM
//    task hram_response;
//        begin
//            while(run_tasks) begin
//                 while(hram_wr_req_n == 1'b1) begin
//                     @(posedge clk);
//                 end
//                hram_idle <= 1'b0;          // RAM is active for a bit
//                repeat(10) @(posedge clk);  // RAM is busy here
//                hram_idle <= 1'b1;          
//            end
//        end
//    endtask
    
    
//    // Monitor task - checks that important conditions are met
//    task monitor;
//        begin
//            while(run_tasks) begin
//                @(posedge clk);
//                if(read_from_input_buf == 1'b1) begin
//                    outstanding_requests <= outstanding_requests + 1;   // Count up the number of pixels requested
//                end
//                if(hram_idle_d == 1'b0 && hram_idle == 1'b1) outstanding_requests <= outstanding_requests - 4;  // When HRAM is done, decrement the request count 
                
//                hram_idle_d <= hram_idle;
//                read_from_input_buf_d <= read_from_input_buf;
//            end
//        end
//    endtask
    
    
endmodule