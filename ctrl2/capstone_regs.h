// -----------------------------------------------------------------------------
// 'capstone' Register Definitions
// Revision: 53
// -----------------------------------------------------------------------------
// Generated on 2019-06-11 at 05:40 (UTC) by airhdl version 2019.06.1
// -----------------------------------------------------------------------------
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
// POSSIBILITY OF SUCH DAMAGE.
// -----------------------------------------------------------------------------

#ifndef CAPSTONE_H
#define CAPSTONE_H

/* Revision number of the 'capstone' register map */
#define CAPSTONE_REVISION 53

/* Default base address of the 'capstone' register map */
#define CAPSTONE_DEFAULT_BASEADDR 0x41210000

/* Register 'control' */
#define CONTROL_OFFSET 0x00000000 /* address offset of the 'control' register */

/* Field  'control.cam_trigger' */
#define CONTROL_CAM_TRIGGER_BIT_OFFSET 0 /* bit offset of the 'cam_trigger' field */
#define CONTROL_CAM_TRIGGER_BIT_WIDTH 1 /* bit width of the 'cam_trigger' field */
#define CONTROL_CAM_TRIGGER_BIT_MASK 0x00000001 /* bit mask of the 'cam_trigger' field */
#define CONTROL_CAM_TRIGGER_RESET 0x0 /* reset value of the 'cam_trigger' field */

/* Field  'control.export_trigger' */
#define CONTROL_EXPORT_TRIGGER_BIT_OFFSET 1 /* bit offset of the 'export_trigger' field */
#define CONTROL_EXPORT_TRIGGER_BIT_WIDTH 1 /* bit width of the 'export_trigger' field */
#define CONTROL_EXPORT_TRIGGER_BIT_MASK 0x00000002 /* bit mask of the 'export_trigger' field */
#define CONTROL_EXPORT_TRIGGER_RESET 0x0 /* reset value of the 'export_trigger' field */

/* Field  'control.buf_wr_trigger' */
#define CONTROL_BUF_WR_TRIGGER_BIT_OFFSET 2 /* bit offset of the 'buf_wr_trigger' field */
#define CONTROL_BUF_WR_TRIGGER_BIT_WIDTH 1 /* bit width of the 'buf_wr_trigger' field */
#define CONTROL_BUF_WR_TRIGGER_BIT_MASK 0x00000004 /* bit mask of the 'buf_wr_trigger' field */
#define CONTROL_BUF_WR_TRIGGER_RESET 0x0 /* reset value of the 'buf_wr_trigger' field */

/* Field  'control.buf_rd_trigger' */
#define CONTROL_BUF_RD_TRIGGER_BIT_OFFSET 3 /* bit offset of the 'buf_rd_trigger' field */
#define CONTROL_BUF_RD_TRIGGER_BIT_WIDTH 1 /* bit width of the 'buf_rd_trigger' field */
#define CONTROL_BUF_RD_TRIGGER_BIT_MASK 0x00000008 /* bit mask of the 'buf_rd_trigger' field */
#define CONTROL_BUF_RD_TRIGGER_RESET 0x0 /* reset value of the 'buf_rd_trigger' field */

/* Field  'control.pclk_threshold' */
#define CONTROL_PCLK_THRESHOLD_BIT_OFFSET 4 /* bit offset of the 'pclk_threshold' field */
#define CONTROL_PCLK_THRESHOLD_BIT_WIDTH 16 /* bit width of the 'pclk_threshold' field */
#define CONTROL_PCLK_THRESHOLD_BIT_MASK 0x000FFFF0 /* bit mask of the 'pclk_threshold' field */
#define CONTROL_PCLK_THRESHOLD_RESET 0x7530 /* reset value of the 'pclk_threshold' field */

/* Field  'control.uart_baud_count' */
#define CONTROL_UART_BAUD_COUNT_BIT_OFFSET 20 /* bit offset of the 'uart_baud_count' field */
#define CONTROL_UART_BAUD_COUNT_BIT_WIDTH 10 /* bit width of the 'uart_baud_count' field */
#define CONTROL_UART_BAUD_COUNT_BIT_MASK 0x3FF00000 /* bit mask of the 'uart_baud_count' field */
#define CONTROL_UART_BAUD_COUNT_RESET 0x364 /* reset value of the 'uart_baud_count' field */

/* Register 'status' */
#define STATUS_OFFSET 0x00000004 /* address offset of the 'status' register */

/* Field  'status.cam_capturing' */
#define STATUS_CAM_CAPTURING_BIT_OFFSET 0 /* bit offset of the 'cam_capturing' field */
#define STATUS_CAM_CAPTURING_BIT_WIDTH 1 /* bit width of the 'cam_capturing' field */
#define STATUS_CAM_CAPTURING_BIT_MASK 0x00000001 /* bit mask of the 'cam_capturing' field */
#define STATUS_CAM_CAPTURING_RESET 0x0 /* reset value of the 'cam_capturing' field */

/* Field  'status.exporting' */
#define STATUS_EXPORTING_BIT_OFFSET 1 /* bit offset of the 'exporting' field */
#define STATUS_EXPORTING_BIT_WIDTH 1 /* bit width of the 'exporting' field */
#define STATUS_EXPORTING_BIT_MASK 0x00000002 /* bit mask of the 'exporting' field */
#define STATUS_EXPORTING_RESET 0x0 /* reset value of the 'exporting' field */

/* Field  'status.fifo_empty' */
#define STATUS_FIFO_EMPTY_BIT_OFFSET 2 /* bit offset of the 'fifo_empty' field */
#define STATUS_FIFO_EMPTY_BIT_WIDTH 1 /* bit width of the 'fifo_empty' field */
#define STATUS_FIFO_EMPTY_BIT_MASK 0x00000004 /* bit mask of the 'fifo_empty' field */
#define STATUS_FIFO_EMPTY_RESET 0x0 /* reset value of the 'fifo_empty' field */

/* Field  'status.fifo_full' */
#define STATUS_FIFO_FULL_BIT_OFFSET 3 /* bit offset of the 'fifo_full' field */
#define STATUS_FIFO_FULL_BIT_WIDTH 1 /* bit width of the 'fifo_full' field */
#define STATUS_FIFO_FULL_BIT_MASK 0x00000008 /* bit mask of the 'fifo_full' field */
#define STATUS_FIFO_FULL_RESET 0x0 /* reset value of the 'fifo_full' field */

/* Field  'status.bram_buf_empty' */
#define STATUS_BRAM_BUF_EMPTY_BIT_OFFSET 4 /* bit offset of the 'bram_buf_empty' field */
#define STATUS_BRAM_BUF_EMPTY_BIT_WIDTH 1 /* bit width of the 'bram_buf_empty' field */
#define STATUS_BRAM_BUF_EMPTY_BIT_MASK 0x00000010 /* bit mask of the 'bram_buf_empty' field */
#define STATUS_BRAM_BUF_EMPTY_RESET 0x0 /* reset value of the 'bram_buf_empty' field */

/* Field  'status.bram_num_entries' */
#define STATUS_BRAM_NUM_ENTRIES_BIT_OFFSET 5 /* bit offset of the 'bram_num_entries' field */
#define STATUS_BRAM_NUM_ENTRIES_BIT_WIDTH 16 /* bit width of the 'bram_num_entries' field */
#define STATUS_BRAM_NUM_ENTRIES_BIT_MASK 0x001FFFE0 /* bit mask of the 'bram_num_entries' field */
#define STATUS_BRAM_NUM_ENTRIES_RESET 0x0 /* reset value of the 'bram_num_entries' field */

/* Field  'status.hwrap_idle' */
#define STATUS_HWRAP_IDLE_BIT_OFFSET 21 /* bit offset of the 'hwrap_idle' field */
#define STATUS_HWRAP_IDLE_BIT_WIDTH 1 /* bit width of the 'hwrap_idle' field */
#define STATUS_HWRAP_IDLE_BIT_MASK 0x00200000 /* bit mask of the 'hwrap_idle' field */
#define STATUS_HWRAP_IDLE_RESET 0x1 /* reset value of the 'hwrap_idle' field */

/* Field  'status.hram_dbg' */
#define STATUS_HRAM_DBG_BIT_OFFSET 22 /* bit offset of the 'hram_dbg' field */
#define STATUS_HRAM_DBG_BIT_WIDTH 8 /* bit width of the 'hram_dbg' field */
#define STATUS_HRAM_DBG_BIT_MASK 0x3FC00000 /* bit mask of the 'hram_dbg' field */
#define STATUS_HRAM_DBG_RESET 0x0 /* reset value of the 'hram_dbg' field */

/* Register 'reset' */
#define RESET_OFFSET 0x00000008 /* address offset of the 'reset' register */

/* Field  'reset.cam_rst_n' */
#define RESET_CAM_RST_N_BIT_OFFSET 0 /* bit offset of the 'cam_rst_n' field */
#define RESET_CAM_RST_N_BIT_WIDTH 1 /* bit width of the 'cam_rst_n' field */
#define RESET_CAM_RST_N_BIT_MASK 0x00000001 /* bit mask of the 'cam_rst_n' field */
#define RESET_CAM_RST_N_RESET 0x0 /* reset value of the 'cam_rst_n' field */

/* Field  'reset.export_rst_n' */
#define RESET_EXPORT_RST_N_BIT_OFFSET 1 /* bit offset of the 'export_rst_n' field */
#define RESET_EXPORT_RST_N_BIT_WIDTH 1 /* bit width of the 'export_rst_n' field */
#define RESET_EXPORT_RST_N_BIT_MASK 0x00000002 /* bit mask of the 'export_rst_n' field */
#define RESET_EXPORT_RST_N_RESET 0x0 /* reset value of the 'export_rst_n' field */

/* Field  'reset.buf_rst_w_n' */
#define RESET_BUF_RST_W_N_BIT_OFFSET 2 /* bit offset of the 'buf_rst_w_n' field */
#define RESET_BUF_RST_W_N_BIT_WIDTH 1 /* bit width of the 'buf_rst_w_n' field */
#define RESET_BUF_RST_W_N_BIT_MASK 0x00000004 /* bit mask of the 'buf_rst_w_n' field */
#define RESET_BUF_RST_W_N_RESET 0x0 /* reset value of the 'buf_rst_w_n' field */

/* Field  'reset.buf_rst_r_n' */
#define RESET_BUF_RST_R_N_BIT_OFFSET 3 /* bit offset of the 'buf_rst_r_n' field */
#define RESET_BUF_RST_R_N_BIT_WIDTH 1 /* bit width of the 'buf_rst_r_n' field */
#define RESET_BUF_RST_R_N_BIT_MASK 0x00000008 /* bit mask of the 'buf_rst_r_n' field */
#define RESET_BUF_RST_R_N_RESET 0x0 /* reset value of the 'buf_rst_r_n' field */

/* Field  'reset.hwrap_rst_n' */
#define RESET_HWRAP_RST_N_BIT_OFFSET 4 /* bit offset of the 'hwrap_rst_n' field */
#define RESET_HWRAP_RST_N_BIT_WIDTH 1 /* bit width of the 'hwrap_rst_n' field */
#define RESET_HWRAP_RST_N_BIT_MASK 0x00000010 /* bit mask of the 'hwrap_rst_n' field */
#define RESET_HWRAP_RST_N_RESET 0x0 /* reset value of the 'hwrap_rst_n' field */

/* Register 'leds' */
#define LEDS_OFFSET 0x0000000C /* address offset of the 'leds' register */

/* Field  'leds.led0' */
#define LEDS_LED0_BIT_OFFSET 0 /* bit offset of the 'led0' field */
#define LEDS_LED0_BIT_WIDTH 3 /* bit width of the 'led0' field */
#define LEDS_LED0_BIT_MASK 0x00000007 /* bit mask of the 'led0' field */
#define LEDS_LED0_RESET 0x7 /* reset value of the 'led0' field */

/* Register 'pxcount' */
#define PXCOUNT_OFFSET 0x00000010 /* address offset of the 'pxcount' register */

/* Field  'pxcount.count' */
#define PXCOUNT_COUNT_BIT_OFFSET 0 /* bit offset of the 'count' field */
#define PXCOUNT_COUNT_BIT_WIDTH 32 /* bit width of the 'count' field */
#define PXCOUNT_COUNT_BIT_MASK 0xFFFFFFFF /* bit mask of the 'count' field */
#define PXCOUNT_COUNT_RESET 0x0 /* reset value of the 'count' field */

/* Register 'imgparam' */
#define IMGPARAM_OFFSET 0x00000014 /* address offset of the 'imgparam' register */

/* Field  'imgparam.width' */
#define IMGPARAM_WIDTH_BIT_OFFSET 0 /* bit offset of the 'width' field */
#define IMGPARAM_WIDTH_BIT_WIDTH 16 /* bit width of the 'width' field */
#define IMGPARAM_WIDTH_BIT_MASK 0x0000FFFF /* bit mask of the 'width' field */
#define IMGPARAM_WIDTH_RESET 0x0 /* reset value of the 'width' field */

/* Field  'imgparam.height' */
#define IMGPARAM_HEIGHT_BIT_OFFSET 16 /* bit offset of the 'height' field */
#define IMGPARAM_HEIGHT_BIT_WIDTH 16 /* bit width of the 'height' field */
#define IMGPARAM_HEIGHT_BIT_MASK 0xFFFF0000 /* bit mask of the 'height' field */
#define IMGPARAM_HEIGHT_RESET 0x0 /* reset value of the 'height' field */

/* Register 'hram_ptr' */
#define HRAM_PTR_OFFSET 0x00000018 /* address offset of the 'hram_ptr' register */

/* Field  'hram_ptr.next_wr_addr' */
#define HRAM_PTR_NEXT_WR_ADDR_BIT_OFFSET 0 /* bit offset of the 'next_wr_addr' field */
#define HRAM_PTR_NEXT_WR_ADDR_BIT_WIDTH 32 /* bit width of the 'next_wr_addr' field */
#define HRAM_PTR_NEXT_WR_ADDR_BIT_MASK 0xFFFFFFFF /* bit mask of the 'next_wr_addr' field */
#define HRAM_PTR_NEXT_WR_ADDR_RESET 0x0 /* reset value of the 'next_wr_addr' field */

/* Register 'control2' */
#define CONTROL2_OFFSET 0x0000001C /* address offset of the 'control2' register */

/* Field  'control2.hwrap_10_bit' */
#define CONTROL2_HWRAP_10_BIT_BIT_OFFSET 0 /* bit offset of the 'hwrap_10_bit' field */
#define CONTROL2_HWRAP_10_BIT_BIT_WIDTH 1 /* bit width of the 'hwrap_10_bit' field */
#define CONTROL2_HWRAP_10_BIT_BIT_MASK 0x00000001 /* bit mask of the 'hwrap_10_bit' field */
#define CONTROL2_HWRAP_10_BIT_RESET 0x0 /* reset value of the 'hwrap_10_bit' field */

/* Field  'control2.hwrap_mode' */
#define CONTROL2_HWRAP_MODE_BIT_OFFSET 1 /* bit offset of the 'hwrap_mode' field */
#define CONTROL2_HWRAP_MODE_BIT_WIDTH 2 /* bit width of the 'hwrap_mode' field */
#define CONTROL2_HWRAP_MODE_BIT_MASK 0x00000006 /* bit mask of the 'hwrap_mode' field */
#define CONTROL2_HWRAP_MODE_RESET 0x0 /* reset value of the 'hwrap_mode' field */

#endif  /* CAPSTONE_H */
