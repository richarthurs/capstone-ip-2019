`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/04/2019 12:17:46 AM
// Design Name: 
// Module Name: export2_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module export2_tb();

  // DUT inputs 
  reg clk;
  reg rst_n;
  
  reg trigger;
  reg [31:0] start_addr;
  reg [31:0] end_addr;
  wire busy;
  
  // HRAM 
  wire hram_rd_req; // output from export module
  reg hram_busy;    // simulated HRAM busy flag
  reg [31:0] hram_data; // DWORD output from HRAM
  wire [31:0] desired_addr; 
  
  // UART
  wire uart_tx_out;
  wire uart_tx_done;
  wire uart_tx_active;
  wire [7:0] uart_curr_byte;
  
  export2#(1) export_inst(
      .clk(clk),
      .rst_n(rst_n),
      
      // AXI IO
      .trigger(trigger),
      .start_addr(start_addr),
      .end_addr(end_addr),
      .busy(busy),
  
      // HRAM 
      .hram_rd_req(hram_rd_req),
      .current_addr(desired_addr),
      .hram_rd_data(hram_data),
      .hram_busy(hram_busy),
  
      // UART outputs
      .uart_tx_out(uart_tx_out),
      .uart_curr_byte(uart_curr_byte),
      
      // Snoops n debugs
      .uart_tx_done_o(uart_tx_done),     // snoop output from embedded uart
      .uart_tx_active_o(uart_tx_active)    // snoop output from embedded uart 
      );
  
    // 100 MHz System Clock
  always begin
      clk <= 1'b1;
      #5;
      clk <= ~clk;
      #5;
  end
  
  // Simulation regs
  reg sim_done;
  
  initial begin
      trigger <= 1'b0;
      rst_n <= 1'b0;
      start_addr <= 'b0;
      end_addr <= 'h8;
      hram_data <= 'h04030201;
      sim_done <= 1'b0;
      
      // Toggle the reset
      @(posedge clk);
      rst_n <= 1'b1;
      @(posedge clk);
      rst_n <= 1'b0;
      @(posedge clk);
      rst_n <= 1'b1;
      @(posedge clk);
      
      fork
        issue_requests();
        hram_mock();
      join
  end
  
    task automatic issue_requests();
    begin
        // trigger the export transfer - a single dword
        @(posedge clk);
        trigger <= 1'b1;
        @(posedge clk);
        trigger <= 1'b0;
        @(posedge clk);
        
        repeat(500) @(posedge clk);
        sim_done <= 1'b1;
    end 
    endtask
  
  
    // HRAM mock - after an HRAM write request is submitted, stay busy for a few clocks
    task automatic hram_mock();
     reg [31:0] rand;
     while(!sim_done) begin
         rand = $random();
         if(hram_rd_req == 1'b1) begin
             hram_busy <= 1'b1;
             repeat(rand[3:0]) @(posedge clk);
             hram_data <= ~hram_data;   // Invert the data so we can see it changing
             hram_busy <= 1'b0;
         end
         else begin
             hram_busy <= 1'b0;
             @(posedge clk);
         end
    end
    endtask
endmodule
