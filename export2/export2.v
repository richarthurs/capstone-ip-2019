`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/3/2019 03:49:57 PM
// Design Name: 
// Module Name: export2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
/*

    Set clocks per bit: 100 MHZ and 115200 baud: = 100,000,000 / 115200 = 870
    
    Export2 module - requests data from HRAM and exports it over a standard UART
*/
//////////////////////////////////////////////////////////////////////////////////

module export2#(
        parameter CLKS_PER_BIT = 870
    )(
    input wire clk,
    input wire rst_n,
    
    // AXI IO
    input wire trigger,
    input wire [31:0] start_addr,
    input wire [31:0] end_addr,
    output wire busy,

    // HRAM 
    output reg hram_rd_req,
    output reg [31:0] current_addr,
    input wire [31:0] hram_rd_data,
    input wire hram_busy,
    input wire hram_rd_rdy,

    // UART outputs
    output wire uart_tx_out,
    
    // Snoops n debugs
    output wire uart_tx_done_o,      // snoop output from embedded uart
    output wire uart_tx_active_o,    // snoop output from embedded uart 
    output reg [7:0] uart_curr_byte,
    output reg [31:0] dword,
    input wire fw_idle,
    output reg dword_rdy
    );
    
    // Request state machine
    localparam IDLE = 3'b000;
    localparam REQUEST = 3'b001;
    localparam WAIT_HRAM = 3'b010;
    localparam TRIGGER_UART = 3'b011;
    localparam CLEANUP = 3'b100;
    
    // UART state machine
    localparam UART_IDLE = 3'b000;
    localparam UART_TRIGGER = 3'b001;
    localparam UART_WAIT = 3'b010;
    
    reg [2:0] request_state;
    reg [2:0] uart_state;
    
    // Data flops
    reg [31:0] final_addr;
    reg [31:0] hram_sr;         // Data flopped in from HRAM that can't be sent to UART yet
    reg [31:0] tx_sr;           // UART transmit shift register
    reg [7:0] tx_byte;          // Data to send to UART
    reg [2:0] byte_num;         // Byte within the DWORD that is being TX'd by the UART
    
    // Setup
    reg trigger_d;              // previous trigger
    reg tx_start;               // trigger for UART
    
    // Tracking
    reg uart_begin_trigger;
    reg uart_busy;
    reg req_busy;
    
    // Snoop assignments
    wire uart_tx_active;
    wire uart_tx_done;
    assign uart_tx_done_o = uart_tx_done;
    assign uart_tx_active_o = uart_tx_active;
    
    
    
    // Instantiate the UART TX
    uart_tx#(CLKS_PER_BIT) 
        m_uart(
        .i_Clock(clk),
        .i_Tx_DV(tx_start),
        .i_Tx_Byte(tx_byte),     
        .o_Tx_Active(uart_tx_active),
        .o_Tx_Serial(uart_tx_out),
        .o_Tx_Done(uart_tx_done)
    );
    
    // Setup defaults
    initial begin
        req_busy <= 1'b0;
        uart_busy <= 1'b0;
        request_state <= IDLE;
        uart_state <= UART_IDLE;
        hram_rd_req <= 1'b0;
        uart_begin_trigger <= 1'b0;
    end
    
    
    // Busy flag
    assign busy = req_busy | uart_busy;
    
    
    // Request state machine
    always @(posedge clk) begin
        if(rst_n == 1'b0) begin     // Handle resets
            trigger_d <= 1'b0;
            req_busy <= 1'b0;
            request_state <= IDLE;
            current_addr <= 'b0;
            hram_rd_req <= 1'b0;
            uart_begin_trigger <= 1'b0;
            dword_rdy <= 1'b0;
        end
        else begin                  // Active mode 
         trigger_d <= trigger;
           
           case(request_state) 
               IDLE: begin
                   req_busy <= 1'b0;
                   uart_begin_trigger <= 1'b0;
                   
                   // Trigger rising edge and everything is ready to go, let's do this
                   if(trigger_d == 1'b0 && trigger == 1'b1 && uart_state == UART_IDLE) begin
                       request_state <= REQUEST;
                       current_addr <= start_addr;
                       final_addr <= end_addr;
                   end
               end
               
               REQUEST: begin
                   req_busy <= 1'b1;
                   if(hram_busy == 1'b0) begin            // Issue the read request from HRAM if it's idle // RA: updated from !hram_busy
                       hram_rd_req <= 1'b1;
                       request_state <= WAIT_HRAM;
                   end
                   else request_state <= REQUEST;  // If HRAM isn't free, hang out here       
               end
               
               WAIT_HRAM: begin
                   hram_rd_req <= 1'b0;
                   if(hram_rd_rdy == 1'b0) request_state <= WAIT_HRAM; // RA: this used to be hram_busy, but switched to rd_rdy since it only pops up for a single clock
                   else begin                      // HRAM is done
                       hram_sr <= hram_rd_data;    // Flop in the hot new data
                       request_state <= TRIGGER_UART;
                   end
               end
               
               TRIGGER_UART: begin
                   // If UART state machine and FW both idle, kick it off with new data
                   if(uart_state == UART_IDLE && dword_rdy == 1'b0 && fw_idle == 1'b1) begin
                       uart_begin_trigger <= 1'b1; // kick off UART state machine
                       dword <= hram_sr; // load new data onto the bus for FW
                       dword_rdy <= 1'b1; // tell FW there's new data
                       request_state <= CLEANUP;
                   end else begin
                       request_state <= TRIGGER_UART;  // Wait for UART to free up
                       uart_begin_trigger <= 1'b0;
                       if(fw_idle == 1'b0) begin 
                           // this condition protects against the case where FW is so slow that the hardware comes back 
                           // around looking to kick off new data again before FW even has a chance to turn off its idle flag
                           dword_rdy <= 1'b0;
                       end
                   end
               end
               
               CLEANUP: begin
                   uart_begin_trigger <= 1'b0;
                   
                   // Handle address incrementing
                   if(current_addr < final_addr) begin
                       current_addr <= current_addr + 4;
                       request_state <= REQUEST;   // Go issue the next HRAM read request
                  end
                       
                   else if(current_addr >= final_addr) begin
                       request_state <= IDLE;
                   end
               end
               
               default: request_state <= IDLE;
           endcase
      end   // if not reset
    end     // always
    
    
    // UART transmission state machine
    always @(posedge clk) begin
        if(rst_n == 1'b0) begin     // Handle resets
           uart_state <= UART_IDLE;
           byte_num <= 1'b0;
           tx_start <= 1'b0;
           uart_busy <= 1'b0;
        end
        
        else begin                  // Active case
           case(uart_state) 
             UART_IDLE: begin
                 byte_num <= 1'b0;
                 tx_start <= 1'b0;
                 uart_busy <= 1'b0;
             
                 if(uart_begin_trigger) begin
                     uart_state <= UART_TRIGGER;
                     tx_sr <= hram_sr;           // Update the dword for the UART state machine
                     uart_busy <= 1'b1;
                 end
             end
             
             UART_TRIGGER: begin
                 if(~uart_tx_active) begin
                     tx_start <= 1'b1;
                     tx_byte <= tx_sr[7:0];   
                     uart_curr_byte <= tx_sr[7:0];
                     byte_num <= byte_num + 1;
                     uart_state <= UART_WAIT;
                 end
                 else uart_state <= UART_TRIGGER;
             end
             
             UART_WAIT: begin
                 tx_start <= 1'b0;
                 
                 if(uart_tx_done == 1'b0) begin
                     uart_state <= UART_WAIT;    // Wait for UART write to finish
                 end
                 else begin
                     if(byte_num != 4) begin      // Shift data over, trigger the next request from the UART if we're not through the dword yet
                         uart_state <= UART_TRIGGER;
                         tx_sr <= tx_sr >> 8;
                     end
                     else begin                  // It was the last byte of the dword, go idle
                         uart_state <= UART_IDLE;
                     end
                 end
             end
             default: uart_state <= UART_IDLE;
         endcase
        end
 
    end
endmodule
