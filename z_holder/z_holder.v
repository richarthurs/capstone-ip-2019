`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/14/2019 09:16:28 AM
// Design Name: 
// Module Name: z_holder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module z_holder(
    output reg cam1_rst_n,
    output reg cam1_pwdn,
    output reg cam2_rst_n,
    output reg cam2_pwdn
    );
    
    initial begin
        cam1_rst_n <= 1'bz;
        cam1_pwdn <= 1'bz;
        cam2_rst_n <= 1'bz;
        cam2_pwdn <= 1'bz;
    end
endmodule
