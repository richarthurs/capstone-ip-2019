-- -----------------------------------------------------------------------------
-- 'capstone' Register Definitions
-- Revision: 69
-- -----------------------------------------------------------------------------
-- Generated on 2019-07-05 at 16:42 (UTC) by airhdl version 2019.06.3
-- -----------------------------------------------------------------------------
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
-- POSSIBILITY OF SUCH DAMAGE.
-- -----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package capstone_regs_pkg is

    -- Type definitions
    type slv1_array_t is array(natural range <>) of std_logic_vector(0 downto 0);
    type slv2_array_t is array(natural range <>) of std_logic_vector(1 downto 0);
    type slv3_array_t is array(natural range <>) of std_logic_vector(2 downto 0);
    type slv4_array_t is array(natural range <>) of std_logic_vector(3 downto 0);
    type slv5_array_t is array(natural range <>) of std_logic_vector(4 downto 0);
    type slv6_array_t is array(natural range <>) of std_logic_vector(5 downto 0);
    type slv7_array_t is array(natural range <>) of std_logic_vector(6 downto 0);
    type slv8_array_t is array(natural range <>) of std_logic_vector(7 downto 0);
    type slv9_array_t is array(natural range <>) of std_logic_vector(8 downto 0);
    type slv10_array_t is array(natural range <>) of std_logic_vector(9 downto 0);
    type slv11_array_t is array(natural range <>) of std_logic_vector(10 downto 0);
    type slv12_array_t is array(natural range <>) of std_logic_vector(11 downto 0);
    type slv13_array_t is array(natural range <>) of std_logic_vector(12 downto 0);
    type slv14_array_t is array(natural range <>) of std_logic_vector(13 downto 0);
    type slv15_array_t is array(natural range <>) of std_logic_vector(14 downto 0);
    type slv16_array_t is array(natural range <>) of std_logic_vector(15 downto 0);
    type slv17_array_t is array(natural range <>) of std_logic_vector(16 downto 0);
    type slv18_array_t is array(natural range <>) of std_logic_vector(17 downto 0);
    type slv19_array_t is array(natural range <>) of std_logic_vector(18 downto 0);
    type slv20_array_t is array(natural range <>) of std_logic_vector(19 downto 0);
    type slv21_array_t is array(natural range <>) of std_logic_vector(20 downto 0);
    type slv22_array_t is array(natural range <>) of std_logic_vector(21 downto 0);
    type slv23_array_t is array(natural range <>) of std_logic_vector(22 downto 0);
    type slv24_array_t is array(natural range <>) of std_logic_vector(23 downto 0);
    type slv25_array_t is array(natural range <>) of std_logic_vector(24 downto 0);
    type slv26_array_t is array(natural range <>) of std_logic_vector(25 downto 0);
    type slv27_array_t is array(natural range <>) of std_logic_vector(26 downto 0);
    type slv28_array_t is array(natural range <>) of std_logic_vector(27 downto 0);
    type slv29_array_t is array(natural range <>) of std_logic_vector(28 downto 0);
    type slv30_array_t is array(natural range <>) of std_logic_vector(29 downto 0);
    type slv31_array_t is array(natural range <>) of std_logic_vector(30 downto 0);
    type slv32_array_t is array(natural range <>) of std_logic_vector(31 downto 0);


    -- Revision number of the 'capstone' register map
    constant CAPSTONE_REVISION : natural := 69;

    -- Default base address of the 'capstone' register map 
    constant CAPSTONE_DEFAULT_BASEADDR : unsigned(31 downto 0) := unsigned'(x"41210000");
    
    -- Register 'control'
    constant CONTROL_OFFSET : unsigned(31 downto 0) := unsigned'(x"00000000"); -- address offset of the 'control' register
    constant CONTROL_CAM_TRIGGER_BIT_OFFSET : natural := 0; -- bit offset of the 'cam_trigger' field
    constant CONTROL_CAM_TRIGGER_BIT_WIDTH : natural := 1; -- bit width of the 'cam_trigger' field
    constant CONTROL_CAM_TRIGGER_RESET : std_logic_vector(0 downto 0) := std_logic_vector'("0"); -- reset value of the 'cam_trigger' field
    constant CONTROL_EXPORT_TRIGGER_BIT_OFFSET : natural := 1; -- bit offset of the 'export_trigger' field
    constant CONTROL_EXPORT_TRIGGER_BIT_WIDTH : natural := 1; -- bit width of the 'export_trigger' field
    constant CONTROL_EXPORT_TRIGGER_RESET : std_logic_vector(1 downto 1) := std_logic_vector'("0"); -- reset value of the 'export_trigger' field
    constant CONTROL_BUF_WR_TRIGGER_BIT_OFFSET : natural := 2; -- bit offset of the 'buf_wr_trigger' field
    constant CONTROL_BUF_WR_TRIGGER_BIT_WIDTH : natural := 1; -- bit width of the 'buf_wr_trigger' field
    constant CONTROL_BUF_WR_TRIGGER_RESET : std_logic_vector(2 downto 2) := std_logic_vector'("0"); -- reset value of the 'buf_wr_trigger' field
    constant CONTROL_BUF_RD_TRIGGER_BIT_OFFSET : natural := 3; -- bit offset of the 'buf_rd_trigger' field
    constant CONTROL_BUF_RD_TRIGGER_BIT_WIDTH : natural := 1; -- bit width of the 'buf_rd_trigger' field
    constant CONTROL_BUF_RD_TRIGGER_RESET : std_logic_vector(3 downto 3) := std_logic_vector'("0"); -- reset value of the 'buf_rd_trigger' field
    constant CONTROL_PCLK_THRESHOLD_BIT_OFFSET : natural := 4; -- bit offset of the 'pclk_threshold' field
    constant CONTROL_PCLK_THRESHOLD_BIT_WIDTH : natural := 16; -- bit width of the 'pclk_threshold' field
    constant CONTROL_PCLK_THRESHOLD_RESET : std_logic_vector(19 downto 4) := std_logic_vector'("0111010100110000"); -- reset value of the 'pclk_threshold' field
    constant CONTROL_UART_BAUD_COUNT_BIT_OFFSET : natural := 20; -- bit offset of the 'uart_baud_count' field
    constant CONTROL_UART_BAUD_COUNT_BIT_WIDTH : natural := 10; -- bit width of the 'uart_baud_count' field
    constant CONTROL_UART_BAUD_COUNT_RESET : std_logic_vector(29 downto 20) := std_logic_vector'("1101100100"); -- reset value of the 'uart_baud_count' field
    
    -- Register 'status'
    constant STATUS_OFFSET : unsigned(31 downto 0) := unsigned'(x"00000004"); -- address offset of the 'status' register
    constant STATUS_CAM_CAPTURING_BIT_OFFSET : natural := 0; -- bit offset of the 'cam_capturing' field
    constant STATUS_CAM_CAPTURING_BIT_WIDTH : natural := 1; -- bit width of the 'cam_capturing' field
    constant STATUS_CAM_CAPTURING_RESET : std_logic_vector(0 downto 0) := std_logic_vector'("0"); -- reset value of the 'cam_capturing' field
    constant STATUS_EXPORTING_BIT_OFFSET : natural := 1; -- bit offset of the 'exporting' field
    constant STATUS_EXPORTING_BIT_WIDTH : natural := 1; -- bit width of the 'exporting' field
    constant STATUS_EXPORTING_RESET : std_logic_vector(1 downto 1) := std_logic_vector'("0"); -- reset value of the 'exporting' field
    constant STATUS_FIFO_EMPTY_BIT_OFFSET : natural := 2; -- bit offset of the 'fifo_empty' field
    constant STATUS_FIFO_EMPTY_BIT_WIDTH : natural := 1; -- bit width of the 'fifo_empty' field
    constant STATUS_FIFO_EMPTY_RESET : std_logic_vector(2 downto 2) := std_logic_vector'("0"); -- reset value of the 'fifo_empty' field
    constant STATUS_FIFO_FULL_BIT_OFFSET : natural := 3; -- bit offset of the 'fifo_full' field
    constant STATUS_FIFO_FULL_BIT_WIDTH : natural := 1; -- bit width of the 'fifo_full' field
    constant STATUS_FIFO_FULL_RESET : std_logic_vector(3 downto 3) := std_logic_vector'("0"); -- reset value of the 'fifo_full' field
    constant STATUS_BRAM_BUF_EMPTY_BIT_OFFSET : natural := 4; -- bit offset of the 'bram_buf_empty' field
    constant STATUS_BRAM_BUF_EMPTY_BIT_WIDTH : natural := 1; -- bit width of the 'bram_buf_empty' field
    constant STATUS_BRAM_BUF_EMPTY_RESET : std_logic_vector(4 downto 4) := std_logic_vector'("0"); -- reset value of the 'bram_buf_empty' field
    constant STATUS_BRAM_NUM_ENTRIES_BIT_OFFSET : natural := 5; -- bit offset of the 'bram_num_entries' field
    constant STATUS_BRAM_NUM_ENTRIES_BIT_WIDTH : natural := 16; -- bit width of the 'bram_num_entries' field
    constant STATUS_BRAM_NUM_ENTRIES_RESET : std_logic_vector(20 downto 5) := std_logic_vector'("0000000000000000"); -- reset value of the 'bram_num_entries' field
    constant STATUS_HWRAP_IDLE_BIT_OFFSET : natural := 21; -- bit offset of the 'hwrap_idle' field
    constant STATUS_HWRAP_IDLE_BIT_WIDTH : natural := 1; -- bit width of the 'hwrap_idle' field
    constant STATUS_HWRAP_IDLE_RESET : std_logic_vector(21 downto 21) := std_logic_vector'("1"); -- reset value of the 'hwrap_idle' field
    constant STATUS_HRAM_DBG_BIT_OFFSET : natural := 22; -- bit offset of the 'hram_dbg' field
    constant STATUS_HRAM_DBG_BIT_WIDTH : natural := 8; -- bit width of the 'hram_dbg' field
    constant STATUS_HRAM_DBG_RESET : std_logic_vector(29 downto 22) := std_logic_vector'("00000000"); -- reset value of the 'hram_dbg' field
    
    -- Register 'reset'
    constant RESET_OFFSET : unsigned(31 downto 0) := unsigned'(x"00000008"); -- address offset of the 'reset' register
    constant RESET_CAM_RST_N_BIT_OFFSET : natural := 0; -- bit offset of the 'cam_rst_n' field
    constant RESET_CAM_RST_N_BIT_WIDTH : natural := 1; -- bit width of the 'cam_rst_n' field
    constant RESET_CAM_RST_N_RESET : std_logic_vector(0 downto 0) := std_logic_vector'("0"); -- reset value of the 'cam_rst_n' field
    constant RESET_EXPORT_RST_N_BIT_OFFSET : natural := 1; -- bit offset of the 'export_rst_n' field
    constant RESET_EXPORT_RST_N_BIT_WIDTH : natural := 1; -- bit width of the 'export_rst_n' field
    constant RESET_EXPORT_RST_N_RESET : std_logic_vector(1 downto 1) := std_logic_vector'("0"); -- reset value of the 'export_rst_n' field
    constant RESET_BUF_RST_W_N_BIT_OFFSET : natural := 2; -- bit offset of the 'buf_rst_w_n' field
    constant RESET_BUF_RST_W_N_BIT_WIDTH : natural := 1; -- bit width of the 'buf_rst_w_n' field
    constant RESET_BUF_RST_W_N_RESET : std_logic_vector(2 downto 2) := std_logic_vector'("0"); -- reset value of the 'buf_rst_w_n' field
    constant RESET_BUF_RST_R_N_BIT_OFFSET : natural := 3; -- bit offset of the 'buf_rst_r_n' field
    constant RESET_BUF_RST_R_N_BIT_WIDTH : natural := 1; -- bit width of the 'buf_rst_r_n' field
    constant RESET_BUF_RST_R_N_RESET : std_logic_vector(3 downto 3) := std_logic_vector'("0"); -- reset value of the 'buf_rst_r_n' field
    constant RESET_HWRAP_RST_N_BIT_OFFSET : natural := 4; -- bit offset of the 'hwrap_rst_n' field
    constant RESET_HWRAP_RST_N_BIT_WIDTH : natural := 1; -- bit width of the 'hwrap_rst_n' field
    constant RESET_HWRAP_RST_N_RESET : std_logic_vector(4 downto 4) := std_logic_vector'("0"); -- reset value of the 'hwrap_rst_n' field
    
    -- Register 'leds'
    constant LEDS_OFFSET : unsigned(31 downto 0) := unsigned'(x"0000000C"); -- address offset of the 'leds' register
    constant LEDS_LED0_BIT_OFFSET : natural := 0; -- bit offset of the 'led0' field
    constant LEDS_LED0_BIT_WIDTH : natural := 3; -- bit width of the 'led0' field
    constant LEDS_LED0_RESET : std_logic_vector(2 downto 0) := std_logic_vector'("111"); -- reset value of the 'led0' field
    
    -- Register 'pxcount'
    constant PXCOUNT_OFFSET : unsigned(31 downto 0) := unsigned'(x"00000010"); -- address offset of the 'pxcount' register
    constant PXCOUNT_COUNT_BIT_OFFSET : natural := 0; -- bit offset of the 'count' field
    constant PXCOUNT_COUNT_BIT_WIDTH : natural := 32; -- bit width of the 'count' field
    constant PXCOUNT_COUNT_RESET : std_logic_vector(31 downto 0) := std_logic_vector'("00000000000000000000000000000000"); -- reset value of the 'count' field
    
    -- Register 'imgparam'
    constant IMGPARAM_OFFSET : unsigned(31 downto 0) := unsigned'(x"00000014"); -- address offset of the 'imgparam' register
    constant IMGPARAM_WIDTH_BIT_OFFSET : natural := 0; -- bit offset of the 'width' field
    constant IMGPARAM_WIDTH_BIT_WIDTH : natural := 16; -- bit width of the 'width' field
    constant IMGPARAM_WIDTH_RESET : std_logic_vector(15 downto 0) := std_logic_vector'("0000000000000000"); -- reset value of the 'width' field
    constant IMGPARAM_HEIGHT_BIT_OFFSET : natural := 16; -- bit offset of the 'height' field
    constant IMGPARAM_HEIGHT_BIT_WIDTH : natural := 16; -- bit width of the 'height' field
    constant IMGPARAM_HEIGHT_RESET : std_logic_vector(31 downto 16) := std_logic_vector'("0000000000000000"); -- reset value of the 'height' field
    
    -- Register 'hram_ptr'
    constant HRAM_PTR_OFFSET : unsigned(31 downto 0) := unsigned'(x"00000018"); -- address offset of the 'hram_ptr' register
    constant HRAM_PTR_NEXT_WR_ADDR_BIT_OFFSET : natural := 0; -- bit offset of the 'next_wr_addr' field
    constant HRAM_PTR_NEXT_WR_ADDR_BIT_WIDTH : natural := 32; -- bit width of the 'next_wr_addr' field
    constant HRAM_PTR_NEXT_WR_ADDR_RESET : std_logic_vector(31 downto 0) := std_logic_vector'("00000000000000000000000000000000"); -- reset value of the 'next_wr_addr' field
    
    -- Register 'control2'
    constant CONTROL2_OFFSET : unsigned(31 downto 0) := unsigned'(x"0000001C"); -- address offset of the 'control2' register
    constant CONTROL2_HWRAP_10_BIT_BIT_OFFSET : natural := 0; -- bit offset of the 'hwrap_10_bit' field
    constant CONTROL2_HWRAP_10_BIT_BIT_WIDTH : natural := 1; -- bit width of the 'hwrap_10_bit' field
    constant CONTROL2_HWRAP_10_BIT_RESET : std_logic_vector(0 downto 0) := std_logic_vector'("0"); -- reset value of the 'hwrap_10_bit' field
    constant CONTROL2_HWRAP_MODE_BIT_OFFSET : natural := 1; -- bit offset of the 'hwrap_mode' field
    constant CONTROL2_HWRAP_MODE_BIT_WIDTH : natural := 2; -- bit width of the 'hwrap_mode' field
    constant CONTROL2_HWRAP_MODE_RESET : std_logic_vector(2 downto 1) := std_logic_vector'("00"); -- reset value of the 'hwrap_mode' field
    constant CONTROL2_CAM1_PWR_BIT_OFFSET : natural := 3; -- bit offset of the 'cam1_pwr' field
    constant CONTROL2_CAM1_PWR_BIT_WIDTH : natural := 1; -- bit width of the 'cam1_pwr' field
    constant CONTROL2_CAM1_PWR_RESET : std_logic_vector(3 downto 3) := std_logic_vector'("0"); -- reset value of the 'cam1_pwr' field
    constant CONTROL2_CAM2_PWR_BIT_OFFSET : natural := 4; -- bit offset of the 'cam2_pwr' field
    constant CONTROL2_CAM2_PWR_BIT_WIDTH : natural := 1; -- bit width of the 'cam2_pwr' field
    constant CONTROL2_CAM2_PWR_RESET : std_logic_vector(4 downto 4) := std_logic_vector'("0"); -- reset value of the 'cam2_pwr' field
    constant CONTROL2_EXPORT_UART_AXI_BIT_OFFSET : natural := 5; -- bit offset of the 'export_uart_axi' field
    constant CONTROL2_EXPORT_UART_AXI_BIT_WIDTH : natural := 1; -- bit width of the 'export_uart_axi' field
    constant CONTROL2_EXPORT_UART_AXI_RESET : std_logic_vector(5 downto 5) := std_logic_vector'("0"); -- reset value of the 'export_uart_axi' field
    constant CONTROL2_CAM1_RST_N_BIT_OFFSET : natural := 6; -- bit offset of the 'cam1_rst_n' field
    constant CONTROL2_CAM1_RST_N_BIT_WIDTH : natural := 1; -- bit width of the 'cam1_rst_n' field
    constant CONTROL2_CAM1_RST_N_RESET : std_logic_vector(6 downto 6) := std_logic_vector'("1"); -- reset value of the 'cam1_rst_n' field
    constant CONTROL2_CAM2_RST_N_BIT_OFFSET : natural := 7; -- bit offset of the 'cam2_rst_n' field
    constant CONTROL2_CAM2_RST_N_BIT_WIDTH : natural := 1; -- bit width of the 'cam2_rst_n' field
    constant CONTROL2_CAM2_RST_N_RESET : std_logic_vector(7 downto 7) := std_logic_vector'("1"); -- reset value of the 'cam2_rst_n' field
    constant CONTROL2_CAM1_PWDN_BIT_OFFSET : natural := 8; -- bit offset of the 'cam1_pwdn' field
    constant CONTROL2_CAM1_PWDN_BIT_WIDTH : natural := 1; -- bit width of the 'cam1_pwdn' field
    constant CONTROL2_CAM1_PWDN_RESET : std_logic_vector(8 downto 8) := std_logic_vector'("0"); -- reset value of the 'cam1_pwdn' field
    constant CONTROL2_CAM2_PWDN_BIT_OFFSET : natural := 9; -- bit offset of the 'cam2_pwdn' field
    constant CONTROL2_CAM2_PWDN_BIT_WIDTH : natural := 1; -- bit width of the 'cam2_pwdn' field
    constant CONTROL2_CAM2_PWDN_RESET : std_logic_vector(9 downto 9) := std_logic_vector'("0"); -- reset value of the 'cam2_pwdn' field
    constant CONTROL2_HWRAP_READ_MODE_BIT_OFFSET : natural := 10; -- bit offset of the 'hwrap_read_mode' field
    constant CONTROL2_HWRAP_READ_MODE_BIT_WIDTH : natural := 1; -- bit width of the 'hwrap_read_mode' field
    constant CONTROL2_HWRAP_READ_MODE_RESET : std_logic_vector(10 downto 10) := std_logic_vector'("0"); -- reset value of the 'hwrap_read_mode' field
    
    -- Register 'export_start_addr'
    constant EXPORT_START_ADDR_OFFSET : unsigned(31 downto 0) := unsigned'(x"00000020"); -- address offset of the 'export_start_addr' register
    constant EXPORT_START_ADDR_ADDR_BIT_OFFSET : natural := 0; -- bit offset of the 'addr' field
    constant EXPORT_START_ADDR_ADDR_BIT_WIDTH : natural := 32; -- bit width of the 'addr' field
    constant EXPORT_START_ADDR_ADDR_RESET : std_logic_vector(31 downto 0) := std_logic_vector'("00000000000000000000000000000000"); -- reset value of the 'addr' field
    
    -- Register 'export_end_addr'
    constant EXPORT_END_ADDR_OFFSET : unsigned(31 downto 0) := unsigned'(x"00000024"); -- address offset of the 'export_end_addr' register
    constant EXPORT_END_ADDR_ADDR_BIT_OFFSET : natural := 0; -- bit offset of the 'addr' field
    constant EXPORT_END_ADDR_ADDR_BIT_WIDTH : natural := 32; -- bit width of the 'addr' field
    constant EXPORT_END_ADDR_ADDR_RESET : std_logic_vector(31 downto 0) := std_logic_vector'("00000000000000000000000000000000"); -- reset value of the 'addr' field

end capstone_regs_pkg;
