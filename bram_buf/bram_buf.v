`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/04/2019 11:30:17 AM
// Design Name: 
// Module Name: bram_buf
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bram_buf#(
    )(
    input wire clk,
    input wire rst_n,
    input wire [7:0] wr_data,
    output reg [7:0] rd_data,  
    output reg buf_empty,     // Active high buffer empty     
    input wire fifo_empty,           // Used to moderate reading data in from FIFO
    input wire rd_request,           // Used to request for read data
    output reg [15:0] write_depth,  // Output showing number of pixels in the buffer
    output reg read_from_fifo,
    input wire wr_trigger,           // Active high trigger
    input wire rd_trigger            // Active high trigger
    );
    
    localparam IDLE = 4'b0000;
    localparam WRITE_EXEC = 4'b0001;
    localparam READ_EXEC = 4'b0010;
    
    
    (*ram_style = "block"*) reg	[7:0]	mem	[0:((1<<16)-1)];      
    reg [3:0] state;
    reg [15:0] ptr;
       
    initial begin
        state = IDLE;
        ptr = 'b0;
        rd_data <= mem[0];
        write_depth <= 'b0;
        buf_empty <= 1'b1;
    end
    
    // Writing
    always @(posedge clk) begin
    
        case(state)
            IDLE: begin
                if(wr_trigger && !rd_trigger) state <= WRITE_EXEC;
                if(!wr_trigger && rd_trigger) state <= READ_EXEC;
                ptr <= 'b0;
                read_from_fifo <= 1'b0;
            end
            
            WRITE_EXEC: begin
                // If fifo attached is not empty, bring in the data
                if(fifo_empty != 1'b1) begin
//                    if(ptr < 16'b1 - 1) begin
                        read_from_fifo <= 1'b1;         
                        mem[ptr] <= wr_data;            // assign the data
                        write_depth <= ptr + 1;         // update the write depth ptr
                        ptr <= ptr + 1;                 // update the pointer
                        buf_empty <= 1'b0;
                        state <= WRITE_EXEC;    
//                    end
// TODO: handle overruns
//                    else state <= IDLE;
                end
                
                if(wr_trigger != 1'b1) state <= IDLE;   // go back to IDLE when trigger deasserted
                
            end
            
            READ_EXEC: begin
                if(rd_request) begin
                    rd_data <= mem[ptr];
                    if(ptr == (write_depth-1) && write_depth > 0) begin // detect end of buffer 
                        buf_empty <= 1'b1;
                        write_depth <= 'b0;
                    end
                    else if(write_depth > 0) ptr <= ptr + 1;    // Not at the end, just keep incrementing
                end
                
                if(!rd_trigger) state <= IDLE;
            end
        endcase
        

    
        
        // if write trigger, camera not 
            
        // if read trigger
    
    
    end
    
    // Reading
    
    
endmodule
