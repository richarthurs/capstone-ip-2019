`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/04/2019 12:12:12 PM
// Design Name: 
// Module Name: bram_buf_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bram_buf_tb();
    
    
    // Generated
    reg clk;
    reg rst_n;
    reg wr_trigger;
    reg rd_trigger;
    reg [7:0] write_data;
    reg fifo_empty;
    reg rd_request;
    
    // Wires for Monitoring
    wire [15:0] write_depth;
    wire [7:0] rd_data;
    wire buf_empty;
    
    
    bram_buf#(
        ) m_bram(
        .clk(clk),
        .rst_n(rst_n),
        .wr_data(write_data),
        .rd_data(rd_data),  
        .buf_empty(buf_empty),     // Active high buffer empty     
        .fifo_empty(fifo_empty),           // Used to moderate reading data in from FIFO
        .rd_request(rd_request),           // Used to request for read data
        .write_depth(write_depth),  // Output showing number of pixels in the buffer
        .wr_trigger(wr_trigger),           // Active high trigger
        .rd_trigger(rd_trigger)            // Active high trigger
        );
        
    
    
    // 50 MHz System Clock
    always begin
        clk <= 1'b1;
        #10;
        clk <= ~clk;
        #10;
    end
    
    // Simulation variables
    reg [15:0] count;
    
    // Testing Loop
    initial begin
        wr_trigger <= 1'b0;
        rd_trigger <= 1'b0;
        write_data <= 1'b0;
        fifo_empty <= 1'b1;
        rd_request <= 1'b0;
        repeat(10) @(posedge clk);
        
        // Trigger the write, toggle FIFO status as would be done IRL
        wr_trigger <= 1'b1;
        repeat(10) @(posedge clk);
        fifo_empty <= 1'b0; // FIFO is not empty, should start reading in the data
        
        for(count = 0; count < 10; count = count + 1) begin
            write_data <= write_data + 1;
            @(posedge clk);
        end
        
        // Go empty for a while
        fifo_empty <= 1'b1;
        repeat(10) @(posedge clk);
        
        
        fifo_empty <= 1'b0;
        for(count = 0; count < 10; count = count + 1) begin
            write_data <= write_data + 1;
            @(posedge clk);
        end
        
        fifo_empty <= 1'b1;

        // Delay, then turn off the write trigger
        repeat(10) @(posedge clk);
        wr_trigger <= 1'b0;
        
        
        
        /////////////
        // READ
        repeat(20) @(posedge clk);
        rd_trigger <= 1'b1;      
        
        repeat(5) @(posedge clk);

        rd_request <= 1'b1;
        repeat(5) @(posedge clk);
        rd_request <= 1'b0;
        repeat(5) @(posedge clk);
        rd_request <= 1'b1; 
        repeat(25) @(posedge clk);
        rd_request <= 1'b0;
        rd_trigger <= 1'b0;
        
        //////////////////////////////////////////////////////////////////////////////////////
        repeat(25) @(posedge clk);

        // Trigger the write, toggle FIFO status as would be done IRL
        wr_trigger <= 1'b1;
        repeat(10) @(posedge clk);
        fifo_empty <= 1'b0; // FIFO is not empty, should start reading in the data
        
        for(count = 30; count < 40; count = count + 1) begin
            write_data <= write_data + 1;
            @(posedge clk);
        end
        
        // Go empty for a while
        fifo_empty <= 1'b1;
        repeat(10) @(posedge clk);
        
        
        fifo_empty <= 1'b0;
        for(count = 80; count < 90; count = count + 1) begin
            write_data <= write_data + 1;
            @(posedge clk);
        end
        
        fifo_empty <= 1'b1;

        // Delay, then turn off the write trigger
        repeat(10) @(posedge clk);
        wr_trigger <= 1'b0;
        
        
        
        /////////////
        // READ
        repeat(20) @(posedge clk);
        rd_trigger <= 1'b1;      
        
        repeat(5) @(posedge clk);

        rd_request <= 1'b1;
        repeat(5) @(posedge clk);
        rd_request <= 1'b0;
        repeat(5) @(posedge clk);
        rd_request <= 1'b1; 
        repeat(25) @(posedge clk);
        rd_request <= 1'b0;
        rd_trigger <= 1'b0;
        
    end
    

    
    
endmodule
