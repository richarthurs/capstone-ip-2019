// -----------------------------------------------------------------------------
// 'led' Register Definitions
// Revision: 3
// -----------------------------------------------------------------------------
// Generated on 2019-02-14 at 05:11 (UTC) by airhdl version 2019.01.1
// -----------------------------------------------------------------------------
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
// POSSIBILITY OF SUCH DAMAGE.
// -----------------------------------------------------------------------------

#ifndef LED_H
#define LED_H

/* Revision number of the 'led' register map */
#define LED_REVISION 3

/* Default base address of the 'led' register map */
#define LED_DEFAULT_BASEADDR 0x41210000

/* Register 'test' */
#define TEST_OFFSET 0x00000000 /* address offset of the 'test' register */

/* Field  'test.led' */
#define TEST_LED_BIT_OFFSET 0 /* bit offset of the 'led' field */
#define TEST_LED_BIT_WIDTH 3 /* bit width of the 'led' field */
#define TEST_LED_BIT_MASK 0x00000007 /* bit mask of the 'led' field */
#define TEST_LED_RESET 0x0 /* reset value of the 'led' field */

#define READ_LED()     (*(volatile uint32_t *)LED_DEFAULT_BASEADDR)
#define WRITE_LED(val) ((*(volatile uint32_t *)LED_DEFAULT_BASEADDR) = (val))

#endif  /* LED_H */
