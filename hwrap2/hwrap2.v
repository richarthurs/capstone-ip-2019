`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Richard Arthurs
// 
// Create Date: 06/08/2019 11:38:39 AM
// Design Name: 
// Module Name: hwrap
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: wrapper for HWRAP_core (our abstraction of HRAM behaviour), and the HRAM interface 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hwrap2(
    input wire clk,

// AXI Reg Connections
input wire rst_n,
input wire [1:0] hwrap_mode,        // Mode setter
output wire hwrap_idle,             // Idle status of wrapper
input wire mode_10_bit,             // 8/10 bit mode setter
output wire [31:0]  hram_ptr,       // Current read/write address of HRAM wrapper
input wire read_mode,               // 1 = read mode

// HWRAP Core Connections
input wire [7:0] in_data,           // Input write data - connect to BUF
input wire in_wr_req,               // valid asserting that BUF has data to feed to HRAM
output wire read_from_input_buf,    // HWRAP requests read from input buffer, active high

// Export module connections
input wire [31:0] hram_addr_read,
input wire hram_rd_req,
output wire [31:0] hram_rd_data,
output wire hram_rd_busy,
output wire hram_rd_rdy,

// HRAM 
output wire hram_clk,
output wire hram_rst_n,
output wire hram_cs_n,
inout wire [7:0] hram_data,  
inout wire hram_rwds,
output wire [7:0] hram_dbg,

// Debug
output wire[2:0] state_out, 
output wire [31:0] hram_wr_data_dbg

);


// States
parameter IDLE = 3'b000;
parameter SEND_REG_WRITE = 3'b001;
parameter WAIT_HRAM_COMPLETE = 3'b010;
parameter READ_OR_WRITE = 3'b011;

// Flag for which module has control
parameter INTERNAL = 2'b00;   // internal control - hwrap 
parameter WRITE = 2'b01;      // WRITE core has control
parameter READ = 2'b10;       // READ core has control

// Regs
reg [2:0] state;
reg [1:0] active_module;      // Drives the muxes to map the active module signals to the HRAM xface
reg rst_n_d;
reg [31:0] hram_addr_internal;
reg [31:0] hram_wr_data_internal;
reg hram_wr_req_internal;
reg hwrap_idle_internal;

// Ties
reg mem_or_reg_tie;
reg [3:0] wr_byte_en_tie;
reg [5:0] rd_num_dwords_tie;
reg rd_req_tie; 
reg [7:0] latency_1x_tie;
reg [7:0] latency_2x_tie;

// Wires to connect modules
wire hram_busy;           // Driven high while xface is busy
wire hram_wr_req;         // Write request to HRAM from HWRAP
wire hram_rd_req_n;       // Read request to HRAM from HWRAP
wire [31:0] hram_addr;    // HRAM read/write address from HWRAP
wire [31:0] hram_wr_data; // HRAM write data from HWRAP
wire burst_wr_rdy;        // HRAM xface is ready for another burst of data

// Connector outputs to mux
wire [31:0] hram_addr_write;
wire [31:0] hram_wr_data_write;
wire hram_wr_req_write;
wire hwrap_idle_write;

// Wires for bidirectional signals
wire [7:0]   dram_dq_in;
wire [7:0]   dram_dq_out;
wire dram_dq_oe_l;
wire dram_rwds_in;
wire dram_rwds_out;
wire dram_rwds_oe_l;

// Wires for polarity flipping

// Bidirectional assigns
assign hram_data = dram_dq_oe_l ? 8'bz : dram_dq_out;     // oe_l goes to 1 for reads - when the module needs an input
assign dram_dq_in = hram_data;
assign hram_rwds = dram_rwds_oe_l ? 1'bz : dram_rwds_out;
assign dram_rwds_in = hram_rwds; 


// HRAM Core Instantiation
hwrap2_core#() hwrap_inst(
    .clk(clk),
    .rst_n(rst_n),
    
    .hwrap_mode(hwrap_mode),        // AXI 
    .idle(hwrap_idle_write),              // AXI
    .in_wr_data(in_data),           
    .mode_10_bit(mode_10_bit),      // AXI
    .in_wr_req(in_wr_req),          // negated buffer empty signal
    .in_buf_rd_req(read_from_input_buf),    
    .hram_ptr(hram_ptr),            // AXI
           
   // Connections to HRAM 
   .hram_addr(hram_addr_write),               // output to hram, HRAM address to write/read
   .hram_wr_data(hram_wr_data_write),         // output to hram, hram write data input
   .hram_wr_req(hram_wr_req_write),           // output to hram, active high hram write request
   .hram_busy(hram_busy),
   .burst_wr_rdy(burst_wr_rdy),
   
   // Debug
   .state(state_out) 
);

 
// HRAM Interface Instantiation
hyper_xface#() hram_if(
    .clk(clk),
    .rst_n(rst_n),      

    .rd_req(hram_rd_req), 
    .wr_req(hram_wr_req),
    .mem_or_reg(mem_or_reg_tie),
    .wr_byte_en(wr_byte_en_tie),
    .rd_num_dwords(rd_num_dwords_tie),
    .addr(hram_addr),
    .wr_d(hram_wr_data),
    .rd_d(hram_rd_data),
    .rd_rdy(hram_rd_rdy),
    .busy(hram_busy), 
    .burst_wr_rdy(burst_wr_rdy),
    .latency_1x(latency_1x_tie),
    .latency_2x(latency_2x_tie),
  
    .dram_dq_in(dram_dq_in),
    .dram_dq_out(dram_dq_out),
    .dram_dq_oe_l(dram_dq_oe_l),
  
    .dram_rwds_in(dram_rwds_in),
    .dram_rwds_out(dram_rwds_out),
    .dram_rwds_oe_l(dram_rwds_oe_l),
  
    .dram_ck(hram_clk),
    .dram_rst_l(hram_rst_n),
    .dram_cs_l(hram_cs_n),
    .sump_dbg(hram_dbg)
);  


initial begin
  mem_or_reg_tie <= 1'b0;             // 0 = DRAM, 1 = register
  wr_byte_en_tie  <= 4'hF;            // 0xF = Write all 4 bytes. 0xE write Bytes 3-1 but not 0.
  rd_num_dwords_tie <= 6'b000001;     // Number of dwords to read
//  rd_req_tie <= 1'b0;                 // No read request
  latency_1x_tie <= 'h12;             // Default latencies according to xface
  latency_2x_tie <= 'h16;
  
  // New
  state <= IDLE;
  active_module <= INTERNAL;
  hram_addr_internal <= 'b0;
  hram_wr_data_internal <= 'b0;
  hram_wr_req_internal <= 1'b0;
  hwrap_idle_internal <= 1'b1;
end


/* MUXES
    - Mux the correct driver onto hram_data/addr based on the active module
    - Muxed signals: outputs from read/write cores to the xface, and hwrap idle signal
*/
assign hram_addr  =      (active_module == INTERNAL) ? hram_addr_internal : 
                        (active_module == WRITE) ? hram_addr_write : 
                        (active_module == READ) ? hram_addr_read : 'b0;
                        
assign hram_wr_data  =  (active_module == INTERNAL) ? hram_wr_data_internal : 
                        (active_module == WRITE) ? hram_wr_data_write : 'b0;
                      
assign hram_wr_req  =   (active_module == INTERNAL) ? hram_wr_req_internal :
                        (active_module == WRITE) ? hram_wr_req_write : 'b0;
                        // TODO: read        
        
assign hwrap_idle =     (active_module == INTERNAL)   ? hwrap_idle_internal :
                        (active_module == WRITE) ? hwrap_idle_write : 'b1;
                        // TODO: read      
                        
//assign hram_wr_req  =   (active_module == INTERNAL) ? hram_wr_req_internal :
//                                                (active_module == WRITE) ? hram_wr_req_write : 'b0;     
                                                
// Misc. assignments
assign hram_rd_busy = hram_busy;
assign hram_wr_data_dbg = hram_wr_data;

/* STATE MACHINE
    - hang around in IDLE by default (does nothing)
    - when reset comes high, issue the write request to set up the latency to be low
    - wait for latency request to finish up
    - READ_WRITE, unlocks write/read access and muxes outputs from write/read core to the xface based on the 
*/
always @(posedge clk) begin
    rst_n_d <= rst_n;
    
    if(rst_n_d == 1'b0 && rst_n == 1'b1) begin // Rising edge of RESET triggers a write to the config register
//        state <= SEND_REG_WRITE;
        state <= READ_OR_WRITE;
    end
    
    case(state)
        IDLE: begin
            hwrap_idle_internal <= 1'b1;
        end
        
        SEND_REG_WRITE: begin               // Kick off the transfer to write the control register
            mem_or_reg_tie <= 1'b1;         // 1 = reg
            hram_addr_internal <= 'h00000800;      // from xface 
            hram_wr_data_internal <= 'h8FE70000;    // variable latency, 3 clk cycles, all others are defaults
            hram_wr_req_internal <= 1'b1;   // Kick off the write request
            hwrap_idle_internal <= 1'b0;    // No longer idle
            state <= WAIT_HRAM_COMPLETE;
        end
        
        WAIT_HRAM_COMPLETE: begin           // Wait for the control register write to finish up
            hram_wr_req_internal <= 1'b0;   // No more request
            if (hram_busy) state <= WAIT_HRAM_COMPLETE;     // Loop here until request is complete
            else begin 
                state <= READ_OR_WRITE;
            end
        end
        
        READ_OR_WRITE: begin                     // Unlock everything for HW write control
            mem_or_reg_tie <= 1'b0;         // 0 = memory
            
            // Update the latencies for the xface
            // These are the previously-cofigured values, not defaults
//            latency_1x_tie <= 'h04;          
//            latency_2x_tie <= 'h0a;
                // Default latencies
                latency_1x_tie <= 'h12;
                latency_2x_tie <= 'h16;
            
            // System level
            hwrap_idle_internal <= 1'b1;    // Idle again
            
            // Set read or write based on the axi flag
            if(read_mode) active_module <= READ;
            else active_module <= WRITE;
        end            
    endcase //endcase

end

endmodule
