 module hwrap_2_wr_tb();
    
    // DUT inputs 
    reg clk;
    reg rst_n;
    reg [1:0] hwrap_mode;
    reg [7:0] in_data;
    reg mode_10_bit;
    reg in_wr_req;
    reg hram_busy;
    reg burst_wr_rdy;
    
    // DUT Outputs
    wire hwrap_idle;
    wire read_from_input_buf;
    wire [31:0] hram_ptr;
    wire hram_wr_req;
    wire [31:0] hram_wr_data;
    wire [31:0] hram_addr;
    
    // Buffer signals
    reg clk_cam;
    reg write_cam;
    reg write_rst_n;
    reg read_rst_n;
    wire buff_full;
    wire [7:0] buf_to_hwrap;
    wire buf_empty;
    
    // Instance the core
    hwrap2_core#() hwrap_core_inst(
        .clk(clk),
        .rst_n(rst_n),
        .hwrap_mode(hwrap_mode),
        .mode_10_bit(mode_10_bit),
        
        // Write Inputs
        .in_wr_data(buf_to_hwrap),
        .in_wr_req(in_wr_req),
    
        // Write Outputs
        .hram_ptr(hram_ptr),
        .idle(hwrap_idle),
        .in_buf_rd_req(read_from_input_buf),
        .hram_wr_req(hram_wr_req),   // pull low for 1 clk to request a write to HRAM
        .hram_wr_data(hram_wr_data),
        .hram_addr(hram_addr),
        
        // HRAM inputs
        .hram_busy(hram_busy),
        .burst_wr_rdy(burst_wr_rdy)
    );
    
    // Instance the buff
    buff#(8,6) buff_inst(
        .i_wclk(clk_cam),
        .i_wrst_n(write_rst_n),
        .i_wr(write_cam), 
        .i_wdata(in_data), 
        .o_wfull(buff_full),
        .i_rclk(clk), 
        .i_rrst_n(read_rst_n), 
        .i_rd(read_from_input_buf), 
        .o_rdata(buf_to_hwrap),
        .o_rempty(buf_empty)
        );       
        
    // Invert the empty signal
    always begin
        assign in_wr_req = ~buf_empty;
        @(posedge clk);
    end
    
    // 100 MHz System Clock
    always begin
        clk <= 1'b1;
        #5;
        clk <= ~clk;
        #5;
    end
    
    // Slower camera clk
    always begin
        clk_cam <= 1'b1;
        #7;
        clk_cam <= 1'b0;
        #7;
    end 
   
   // Sim regs
   reg sim_done;
   reg[6:0] num_pixels;
   
   initial begin
   // Setup static inputs 
   hwrap_mode <= 2'b0;
   mode_10_bit <= 1'b0;
   hram_busy <= 1'b0;
   rst_n <= 1'b1;
   sim_done <= 1'b0;
   in_data <= 'hF0;
   read_rst_n <= 1'b1;
   write_rst_n <= 1'b1;
   write_cam <= 1'b0;
   burst_wr_rdy <= 1'b0;
   
   // Setup sim variables
   num_pixels <= 'b0;
   
   // Assert a reset
   @(posedge clk);
   rst_n <= 1'b0;
   read_rst_n <= 1'b0;
   write_rst_n <= 1'b0;
   @(posedge clk);
   rst_n <= 1'b1;
   read_rst_n <= 1'b1;
   write_rst_n <= 1'b1;
   @(posedge clk);
   @(posedge clk);
   @(posedge clk);

   fork
    hram_mock();
    fake_camera();
   join
      
   end
   
   // Data feeder task - increment data in a known pattern so we can see if anything is dropped
   task fake_camera();
        while(!sim_done) begin
            new_pixel();
        end
   endtask
   
   task automatic random_wait();
    reg[31:0] delay;
    begin
        delay = $random();
        repeat(delay[1:0]) @(posedge clk_cam);
    end
   endtask
   
   
   // Generate a new pixel - 50% chance of fixed delay (fast, mimics cam during a line) or variable delay 
   task automatic new_pixel();
    reg[31:0] rand; // Random variable used for delays/deciding whether to do a random delay or not
     begin
        // A couple clocks to sync up
        @(posedge clk_cam);
        @(posedge clk_cam);

        while(!sim_done && num_pixels < 'd16) begin
            rand = $random();

            if(rand[31] == 1'b1) begin  // Request a new px immediately
                write_cam <= 1'b1;
                increment_pixel();
                @(posedge clk_cam);    
            end
            else begin                  // Hold the write down briefly
                write_cam <= 1'b1;
                increment_pixel();
                @(posedge clk_cam);
                write_cam <= 1'b0;
               repeat(rand[3:0]) @(posedge clk_cam);    // Wait a random amount of time
            end
        end // sim done
     end // begin
   endtask
    
   
   // Increment the pixels up to 8, then return to 1 - allows you to see that pixels are going in/out in the correct order
   task increment_pixel();
    begin
        num_pixels <= num_pixels + 1;
        if(in_data < 'hF8) begin
            in_data <= in_data + 1;
        end
        else in_data <= 'hF1; 
        end
   endtask
   
   
   // HRAM mock - after an HRAM write request is submitted, stay busy for a few clocks
   task automatic hram_mock();
    reg [31:0] rand;
    while(!sim_done) begin
        rand = $random();
        if(hram_wr_req == 1'b1) begin
            hram_busy <= 1'b1;
            repeat(rand[2:0]) @(posedge clk);
        end
        else begin
            hram_busy <= 1'b0;
            @(posedge clk);
        end
   end
   endtask
    
endmodule