`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/28/2019 12:43:02 PM
// Design Name: 
// Module Name: hwrap_xface_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
/*

    This testbench features pixel generation getting fed into a buffer, then through the hwrap and out the HRAM
        - random pixel delays to simulate latencies from new line delays
        - HRAM is not mocked in the write case, this just results in the max HRAM latencies being added by the controller
       
*/
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hwrap_xface_tb();
  
  // DUT inputs 
  reg clk;
  reg rst_n;
  reg [1:0] hwrap_mode;
  reg [9:0] in_data;
  reg mode_10_bit;
  reg in_wr_req;
  
  // DUT Outputs
  wire hwrap_idle;
  wire read_from_input_buf;
  wire [31:0] hram_ptr;
  wire hram_wr_req;
  wire [31:0] hram_wr_data;
  wire [31:0] hram_addr;
  
  // Xface outputs
  wire [31:0] hram_rd_data;
  wire hram_rd_ready;
  wire hram_busy;
  wire burst_wr_rdy;
  
  // Xface HRAM connections
  wire hram_clk;
  wire hram_cs_n;
  wire hram_rst_n;
  wire [7:0] dram_dq_out;
  wire dram_rwds_out;
  
  // Buffer signals
  reg clk_cam;
  reg write_cam;
  reg write_rst_n;
  reg read_rst_n;
  wire buff_full;
  wire [9:0] buf_to_hwrap;
  wire buf_empty;
  
  // Instance the core
  hwrap2_core#() hwrap_core_inst(
      .clk(clk),
      .rst_n(rst_n),
      .hwrap_mode(hwrap_mode),
      .mode_10_bit(mode_10_bit),
      
      // Write Inputs
      .in_wr_data(buf_to_hwrap),
      .in_wr_req(in_wr_req),
  
      // Write Outputs
      .hram_ptr(hram_ptr), 
      .idle(hwrap_idle),
      .in_buf_rd_req(read_from_input_buf),
      .hram_wr_req(hram_wr_req),   // push high for 1 clk to request write from HRAM
      .hram_wr_data(hram_wr_data),
      .hram_addr(hram_addr),
      
      // HRAM inputs
      .hram_busy(hram_busy),
      .burst_wr_rdy(burst_wr_rdy)
  );
  
   // Ties
   reg mem_or_reg_tie;          // 0 = DRAM, 1 = reg
   reg [3:0] wr_byte_en_tie;    // Tie to F = 1 dword
   reg [5:0] rd_num_dwords_tie; // Tie to 1
   reg rd_req_tie;              // Read request = 0, this TB only does writes
   reg dram_rwds_in_tie;        // Tie this low so that internal FSM won't get confuzzled
   reg [7:0] latency_1x_tie;
   reg [7:0] latency_2x_tie;
   
   initial begin
     mem_or_reg_tie <= 1'b0;            // 0 = DRAM, 1 = reg
     wr_byte_en_tie  <= 4'hF;           // 0xF = Write all 4 bytes. 0xE write Bytes 3-1 but not 0.
     rd_num_dwords_tie <= 6'b000001;    // Number of dwords to read, example 0x01.
     rd_req_tie <= 1'b0;                // No read request      
     dram_rwds_in_tie <= 1'b0;          // RWDS output from HRAM = 0
     latency_1x_tie <= 'h12;            // Default latencies according to xface
     latency_2x_tie <= 'h16;
   end  
   
      // HRAM Interface Instantiation
      hyper_xface#() hram_if(
          .clk(clk),
          .rst_n(rst_n),     
  
          .rd_req(rd_req_tie), 
          .wr_req(hram_wr_req),
          .mem_or_reg(mem_or_reg_tie), 
          .wr_byte_en(wr_byte_en_tie),
          .rd_num_dwords(rd_num_dwords_tie),
          .addr(hram_addr),     
          .wr_d(hram_wr_data),  
          .rd_d(hram_rd_data),
          .rd_rdy(hram_rd_ready),
          .burst_wr_rdy(burst_wr_rdy),
          .busy(hram_busy),  
  //        output reg          burst_wr_rdy, // RA: not required
          .latency_1x(latency_1x_tie),
          .latency_2x(latency_2x_tie),
        
//          .dram_dq_in(dram_dq_in),
          .dram_dq_out(dram_dq_out),
//          .dram_dq_oe_l(dram_dq_oe_l),
        
          .dram_rwds_in(dram_rwds_in_tie),
          .dram_rwds_out(dram_rwds_out),
//          .dram_rwds_oe_l(dram_rwds_oe_l),
        
          .dram_ck(hram_clk),
          .dram_rst_l(hram_rst_n),
          .dram_cs_l(hram_cs_n)
//          .sump_dbg(hram_dbg)
      );


  // Instance the buff
  buff#(10,6) buff_inst(
      .i_wclk(clk_cam),
      .i_wrst_n(write_rst_n),
      .i_wr(write_cam), 
      .i_wdata(in_data), 
      .o_wfull(buff_full),
      .i_rclk(clk), 
      .i_rrst_n(read_rst_n), 
      .i_rd(read_from_input_buf), 
      .o_rdata(buf_to_hwrap),
      .o_rempty(buf_empty)
      );       
           
  // Invert the empty signal
  always begin
      assign in_wr_req = ~buf_empty;
      @(posedge clk);
  end
  
  // 100 MHz System Clock
  always begin
      clk <= 1'b1;
      #5;
      clk <= ~clk;
      #5;
  end
  
  // Slower camera clk
  always begin
      clk_cam <= 1'b1;
      #40;
      clk_cam <= 1'b0;
      #40;
  end 
 
 // Sim regs
 reg sim_done;
 reg[6:0] num_pixels;
 
  // Setup static inputs 
 initial begin
     hwrap_mode <= 2'b0;
     mode_10_bit <= 1'b0;
     rst_n <= 1'b1;
     sim_done <= 1'b0;
     in_data <= 'b0;
     read_rst_n <= 1'b1;
     write_rst_n <= 1'b1;
     write_cam <= 1'b0;
     
     // Setup sim variables
     num_pixels <= 'b0;
     
     // Assert a reset
     @(posedge clk);
     rst_n <= 1'b0;
     read_rst_n <= 1'b0;
     write_rst_n <= 1'b0;
     @(posedge clk);
     rst_n <= 1'b1;
     read_rst_n <= 1'b1;
     write_rst_n <= 1'b1;
     @(posedge clk);
     @(posedge clk);
     @(posedge clk);
    
     fork
    //  hram_mock();
      fake_camera();
     join
        
 end
 
 // Data feeder task - increment data in a known pattern so we can see if anything is dropped
 task fake_camera();
      while(!sim_done) begin
          new_pixel();
      end
 endtask 
 
 task automatic random_wait();
  reg[31:0] delay;
  begin
      delay = $random();
      repeat(delay[1:0]) @(posedge clk_cam);
  end
 endtask
 
 
 // Generate a new pixel - 50% chance of fixed delay (fast, mimics cam during a line) or variable delay 
 task automatic new_pixel();
  reg[31:0] rand; // Random variable used for delays/deciding whether to do a random delay or not
   begin
      // A couple clocks to sync up
      @(posedge clk_cam);
      @(posedge clk_cam);

      while(!sim_done && num_pixels < 'd16) begin
          rand = $random();

          if(rand[31] == 1'b1) begin  // Request a new px immediately
              write_cam <= 1'b1;
              increment_pixel();
              @(posedge clk_cam);
  
          end
//          else begin                  // Hold the write down briefly
//              write_cam <= 1'b1;
//              increment_pixel();
//              @(posedge clk_cam);
//              write_cam <= 1'b0;
//             repeat(rand[2:0]) @(posedge clk_cam);    // Wait a random amount of time
//          end
      end // sim done
      write_cam <= 1'b0;
   end // begin
 endtask
 
 
 // Increment the pixels up to 8, then return to 1 - allows you to see that pixels are going in/out in the correct order
 task increment_pixel();
  begin
      num_pixels <= num_pixels + 1;
      if(in_data < 'd8) begin
          in_data <= in_data + 1;
      end
      else in_data <= 'b1;
      end
 endtask
  
  
//  // HRAM mock - after an HRAM write request is submitted, stay busy for a few clocks
//  task automatic hram_mock();
//   reg [31:0] rand;
//   while(!sim_done) begin
//       rand = $random();
//       if(hram_wr_req_n == 1'b0) begin
//           hram_idle <= 1'b0;
//           repeat(rand[3:0]) @(posedge clk);
//       end
//       else begin
//           hram_idle <= 1'b1;
//           @(posedge clk);
//       end
//  end
//  endtask
   
endmodule