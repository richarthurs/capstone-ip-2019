`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/22/2019 03:33:20 PM
// Design Name: 
// Module Name: hwrap2_core
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hwrap2_core(
    input wire clk,
    
    // AXI controllable
    input wire rst_n,
    input wire[1:0] hwrap_mode,
    input wire mode_10_bit,
    
    // Write Inputs
    input wire[7:0] in_wr_data,
    input wire in_wr_req,
    output reg[31:0] hram_ptr,

    
    // Write Outputs
    output reg idle, 
    output reg in_buf_rd_req,
    output reg hram_wr_req,   // Push high 1 clk to trigger a write to HRAM
    output reg [31:0] hram_wr_data,   
    output reg [31:0] hram_addr, 
   
    // HRAM inputs
    input wire hram_busy,       // output from xface
    input wire burst_wr_rdy,       // output from xface
    
    // Debug outputs
    output reg[2:0] state       // this is the internal state too
    );
    
    // Params/flags
    parameter WRITE_MODE = 2'b00;
    parameter READ_MODE = 2'b01;
    parameter FLUSH_MODE = 2'b10;

    // States
    parameter IDLE = 3'b000;
    parameter READ_IN_DATA = 3'b001;
    parameter SEND_DATA = 3'b010;
    parameter PAUSE = 3'b011;
    parameter INCREMENT_ADDR = 3'b100;
    
    reg [2:0] byte_count;
    reg [31:0] dword_sr;    // Internal DWORD shift register - LSByte is first byte this module receives 
    initial begin
        byte_count <= 'b0;
    end
    
    always @(posedge clk) begin
    
    // Reset Handler
        if(rst_n == 1'b0) begin
            hram_ptr <= 'b0;        // reset pointer 
            state <= IDLE;
            hram_wr_req <= 1'b0;    // no write request to HRAM
            in_buf_rd_req <= 1'b0;  // no read request
            byte_count <= 3'b0;
            hram_addr <= 'b0;
            dword_sr <= 'b0;
        end
        
    // Regular Operation
        else begin
            if(hwrap_mode == WRITE_MODE) begin
                case(state)
                    IDLE: begin
                        hram_wr_req <= 1'b0;    // no write req to HRAM
                        if(in_wr_req) begin     // if connected buf is not full, go to the step where pixels are registered
                            state <= READ_IN_DATA;
                            in_buf_rd_req <= 1'b1;  // assert a read request - there is a 1 clk read latency before the data is valid, so pick it up in the next state
                            dword_sr <= dword_sr >> 8;
                        end
                        else begin
                            state <= IDLE;      // buf is empty, hang out here
                            in_buf_rd_req <= 1'b0;  
                        end
                    end
                    READ_IN_DATA: begin 
                        if(in_wr_req == 1'b1) begin // Register the data in the shift reg if it's valid (it should always be valid)
                            in_buf_rd_req <= 1'b0;  // disable the read request
                            dword_sr <= {in_wr_data[7:0], dword_sr[23:0]};  // Shift in the data
                            byte_count <= byte_count + 1;                   
                            
                            if(byte_count == 3) state <= SEND_DATA;         // On the last byte, prep to send it out
                            else state <= IDLE;                             // Back to idle so we can wait or request the next data byte
                        end
                        else state <= IDLE;
                    end
                    
                    SEND_DATA: begin  
                        byte_count <= 'b0;
                        if(hram_busy == 1'b0 || burst_wr_rdy) begin     // HRAM is not busy (typical on first write) or xface is ready for a burst, initiate the write
                            state <= PAUSE;   
                            hram_wr_req <= 1'b1; 
                            hram_ptr <= hram_ptr + 'd4;
                        end
                        else begin                      // Wait for HRAM to become not busy
                            state <= SEND_DATA;
                        end                    
                    end
                    
                    PAUSE: begin    // Brief pause for addr/data stability - not sure if required
                        hram_wr_req <= 1'b0; 
                        state <= INCREMENT_ADDR;
                    end
                    
                    INCREMENT_ADDR: begin               // only increment addr for HRAM after it has been registered by HRAM
                        hram_addr <= hram_addr + 'd4;   
                        state <= IDLE;
                    end
                    default: state <= IDLE; 
                endcase
            
            end // write mode
        end // regular operation
    end // always clk
    
    
    // Idle flag
    always @(posedge clk) begin
        if(rst_n == 1'b0) idle <= 1'b0;
        else idle <= (state == IDLE);
    end

    // Data output for HRAM update
    always @(posedge clk) begin
        if(rst_n == 1'b0) begin
            hram_wr_data <= 'b0;
        end
        else begin
           if(state == SEND_DATA) hram_wr_data <= dword_sr;
        end
    end    
    
endmodule
