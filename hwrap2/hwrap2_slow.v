`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Richard Arthurs
// 
// Create Date: 06/08/2019 11:38:39 AM
// Design Name: 
// Module Name: hwrap2_slow
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: wrapper for HWRAP_core (our abstraction of HRAM behaviour), and the HRAM interface 
// 
/*
	- this is the slow version, does not include a state machine wrapper to issue a latency reduction request upon reset
*/

// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hwrap2_slow(
    input wire clk,
    
    // AXI Reg Connections
    input wire rst_n,
    input wire [1:0] hwrap_mode,        // Mode setter
    output wire hwrap_idle,             // Idle status of wrapper
    input wire mode_10_bit,             // 8/10 bit mode setter
    output wire [31:0]  hram_ptr,       // Current read/write address of HRAM wrapper
    
    // HWRAP Core Connections
    input wire [7:0] in_data,           // Input write data - connect to BUF
    input wire in_wr_req,               // valid asserting that BUF has data to feed to HRAM
    output wire read_from_input_buf,    // HWRAP requests read from input buffer, active high
    
    // HRAM 
    output wire hram_clk,
    output wire hram_rst_n,
    output wire hram_cs_n,
    inout wire [7:0] hram_data,  
    inout wire hram_rwds,
    output wire [7:0] hram_dbg,
    
    // Debug
    output wire[2:0] state_out
  );
  
  // Ties
  reg mem_or_reg_tie;
  reg [3:0] wr_byte_en_tie;
  reg [5:0] rd_num_dwords_tie;
  reg rd_req_tie; 
  reg [7:0] latency_1x_tie;
  reg [7:0] latency_2x_tie;
  
  initial begin
    mem_or_reg_tie <= 1'b0;             // 0 = DRAM, 1 = register
    wr_byte_en_tie  <= 4'hF;            // 0xF = Write all 4 bytes. 0xE write Bytes 3-1 but not 0.
    rd_num_dwords_tie <= 6'b000001;     // Number of dwords to read
    rd_req_tie <= 1'b0;                 // No read request
    latency_1x_tie <= 'h12;             // Default latencies according to xface
    latency_2x_tie <= 'h16;
  end
  
  // Wires to connect modules
  wire hram_busy;           // Driven high while xface is busy
  wire hram_wr_req;         // Write request to HRAM from HWRAP
  wire hram_rd_req_n;       // Read request to HRAM from HWRAP
  wire [31:0] hram_addr;    // HRAM read/write address from HWRAP
  wire [31:0] hram_wr_data; // HRAM write data from HWRAP
  wire [31:0] hram_rd_data; // HRAM read data from HWRAP
  wire hram_rd_ready;       // HRAM read data is valid
  
  // Wires for bidirectional signals
  wire [7:0]   dram_dq_in;
  wire [7:0]   dram_dq_out;
  wire dram_dq_oe_l;
  wire dram_rwds_in;
  wire dram_rwds_out;
  wire dram_rwds_oe_l;
  
  // Wires for polarity flipping
  
  // Bidirectional assigns
  assign hram_data = dram_dq_oe_l ? 8'bz : dram_dq_out;     // oe_l goes to 1 for reads - when the module needs an input
  assign dram_dq_in = hram_data;
  assign hram_rwds = dram_rwds_oe_l ? 1'bz : dram_rwds_out;
  assign dram_rwds_in = hram_rwds; 
    
  
    // HRAM Core Instantiation
    hwrap2_core#() hwrap_inst(
        .clk(clk),
        .rst_n(rst_n),
        
        .hwrap_mode(hwrap_mode),        // AXI 
        .idle(hwrap_idle),              // AXI
        .in_wr_data(in_data),           
        .mode_10_bit(mode_10_bit),      // AXI
        .in_wr_req(in_wr_req),          // negated buffer empty signal
        .in_buf_rd_req(read_from_input_buf),    
        .hram_ptr(hram_ptr),            // AXI
               
       // Connections to HRAM 
       .hram_addr(hram_addr),               // output to hram, HRAM address to write/read
       .hram_wr_data(hram_wr_data),         // output to hram, hram write data input
       .hram_wr_req(hram_wr_req),           // output to hram, active high hram write request
//       .hram_rd_data(hram_rd_data),       // input from HRAM, read data
//       .hram_rd_rdy(hram_rd_ready),       // input from HRAM, read data is valid
//       .hram_rd_req(hram_rd_req),         // output to HRAM, read request is present
       .hram_busy(hram_busy),
       
       // Debug
       .state(state_out) 
    );
    
     
    // HRAM Interface Instantiation
    hyper_xface#() hram_if(
        .clk(clk),
        .rst_n(rst_n),      

        .rd_req(rd_req_tie), 
        .wr_req(hram_wr_req),
        .mem_or_reg(mem_or_reg_tie),
        .wr_byte_en(wr_byte_en_tie),
        .rd_num_dwords(rd_num_dwords_tie),
        .addr(hram_addr),
        .wr_d(hram_wr_data),
        .rd_d(hram_rd_data),
        .rd_rdy(hram_rd_ready),
        .busy(hram_busy), 
//        output reg          burst_wr_rdy, // RA: not required
        .latency_1x(latency_1x_tie),
        .latency_2x(latency_2x_tie),
      
        .dram_dq_in(dram_dq_in),
        .dram_dq_out(dram_dq_out),
        .dram_dq_oe_l(dram_dq_oe_l),
      
        .dram_rwds_in(dram_rwds_in),
        .dram_rwds_out(dram_rwds_out),
        .dram_rwds_oe_l(dram_rwds_oe_l),
      
        .dram_ck(hram_clk),
        .dram_rst_l(hram_rst_n),
        .dram_cs_l(hram_cs_n),
        .sump_dbg(hram_dbg)
    );  
endmodule
